rails g scaffold Casen year:integer segmento:integer folio:string o:integer region:integer comuna:integer zona:integer estrato:integer varstrat:integer varunit:integer expr_r2:integer expc_r2:integer expr_r2sat:integer expr_r2fin:integer pobviv2002:integer mr2viv2011:integer fmr2:float postpob:integer postid:integer pco1:integer viv:integer hog:integer tot_hogviv:integer o_viv:integer sexo:integer edad:integer ecivil:integer pareja:integer nucleo:integer pco2:integer numper:integer h11:integer h11e_esp:integer e1:integer e2a:integer e2b:integer e2c:integer e3:integer e4:integer e5:integer e6a:integer e6b:integer e6c:integer e7:integer e8dv:integer e8nom:string e8dir:string e8com:integer e8rbd:integer e8te:integer e9:integer e10:integer e11a:integer e11b:integer e11c:integer e11d:integer e12a:integer e12b1:integer e12b2:integer e13pbu:integer e13pbt:integer e13pbd:integer e13pbm:integer e13bu:integer e13bt:integer e13bd:integer e13bm:integer e13bp:integer e13mu:integer e13mt:integer e13md:integer e13mm:integer e14a:integer e14b:integer e15:integer e16:integer e17t1:integer e17t2:integer e18t1m:integer e18t1p:integer e18t2m:integer e18t2p:integer e0:integer o1:integer o2:integer o3:integer o4:integer o5:integer o6:integer o7r1:integer o7r2:integer o8:integer oficio4:integer o10:integer o11:integer o12:integer o13:integer o14:integer o15:integer o16:integer o17:integer o18:integer o19:integer o20:integer rama4_sub:integer o22:integer rama4:integer o24:integer o25:integer o26:integer o27:integer o28:integer o29:integer o30:integer o0:integer y21:integer y28a:integer y28b:integer y28c:integer y28d:integer y28e:integer y29a:integer y29b1:integer y29b2:integer y29b3:integer y29b4:integer y29b5:integer y30a:integer y30b:integer y30c:integer y30d:integer y30e:integer y0:integer s1:integer s2a:integer s2b:integer s2c:integer s3:integer s4:integer s5:integer s6:integer s7:integer s8:integer s9:integer s10:integer s11:integer s12:integer s13:integer s14:integer s15:integer s16:integer s17:integer s18a:integer s18b:integer s19:integer s20:integer s21:integer s22:integer s23:integer s24a:integer s24b:integer s24c:integer s25a:integer s25b:integer s25c:integer s26a:integer s26b:integer s26c:integer s27a:integer s27b:integer s27c:integer s28a:integer s28b:integer s28c:integer s29a:integer s29b:integer s29c:integer s30a:integer s30b:integer s30c:integer s30d:integer s31a:integer s31b:integer s31c:integer s31d:integer s32a:integer s32b:integer s32c:integer s32d:integer s33a:integer s33b:integer s33c:integer s33d:integer s34:integer s35:integer s36:integer s37t1:integer s37t2:integer s37t3:integer s38o1:integer s38o2:integer s38o3:integer s39a:integer s39b:integer s39c:integer s39d:integer s40a:integer s40b:integer s40c:integer s40d:integer s40e:integer s40f:integer s40g:integer s41a:integer s41b:integer s0:integer r1a:integer r1c_cod:integer r1b:integer r1p_cod:integer r2:integer r2c_cod:integer r2p_cod:integer r3:integer r4mn:integer r4mc:integer r5pn:integer r5pc:integer r6:integer r7:integer r8:integer r9:integer r10:integer r11a:integer r11b:integer r12:integer r13a:integer r13b:integer r13c:integer r13d:integer r13e:integer r13f:integer r14a:integer r14b:integer r14c:integer r14d:integer r14e:integer r15:integer r16:integer r17:integer r18a:integer r18b:integer r18c:integer r18d:integer r18e:integer r18f:integer r18g:integer r18h:integer r19:integer r20:integer r0:integer v1:integer v2:integer v2frent:integer v2fo:integer v3:integer v4:integer v5:integer v5m:integer v6:integer v7:integer v8:integer v9:integer v10:integer v11:integer v12:integer v13:integer v14:integer v15:integer v16a:integer v16b:integer v16c:integer v17:integer v18:integer v19:integer v19r:integer v20:integer v21:integer v22:integer v23:integer v23r:integer v24:integer v25:integer v26a:integer v26b:integer v26c:integer v26d:integer v26e:integer v27a:integer v27b:integer v27c:integer v27d:integer v27e:integer v28:integer v29:integer v30:integer v31:integer v32:integer v33:integer v34:integer v35:integer v36:integer v37:integer v38:integer v39:integer v0a:integer v0b:integer esc:integer educ:integer asiste:integer depen:integer activ:integer oficio1:integer rama1_sub:integer rama1:integer submuestra:integer corte:integer qaut:integer daut:integer daur:integer qaur:integer yopraj:integer yoprhaj:integer ytrabaj:integer ytrabhaj:integer ypensaj:integer ypenshaj:integer ypresaj:integer ypreshaj:integer ysfamaj:integer ysfamhaj:integer y2401aj:integer y2401haj:integer ycesaj:integer yceshaj:integer ybpfeaj:integer ybpfehaj:integer yfamaj:integer yfamhaj:integer yotpaj:integer yotphaj:integer yjubaj:integer yjubhaj:integer yvitaj:integer yvithaj:integer yinvaj:integer yinvhaj:integer ymonaj:integer ymonhaj:integer yorfaj:integer yorfhaj:integer y2301aj:integer y2301haj:integer y2501aj:integer y2501haj:integer y2502aj:integer y2502haj:integer y2503aj:integer y2503haj:integer y2504aj:integer y2504haj:integer yautaj:integer yauthaj:integer yoautaj:integer yoauthaj:integer ymoneaj:integer ymonehaj:integer ysubaj:integer ysubhaj:integer ytotaj:integer ytothaj:integer yaimhaj:integer ypcauthaj:integer ypcmonhaj:integer indmat:integer indsan:integer cviv:integer iae:integer iai:integer hacinamiento:integer mes_ent:integer finde:integer hogar:integer

rails g scaffold Municipio comuna:integer name:string population:integer edad_mean:float edad_median:float edad_standard_deviation:float ecivil_mode:float nucleo_mean:float nucleo_median:float nucleo_standard_deviation:float pco2_mode:float numper_mean:float numper_median:float numper_standard_deviation:float e6a_mode:float e7_mean:float e7_median:float e7_standard_deviation:float e7_mode:float educ_mean:float educ_median:float educ_standard_deviation:float educ_mode:float asiste_mean:float asiste_median:float asiste_standard_deviation:float

rails g scaffold Layer description:string maxValue:float minValue:float name:string opacity:float show:boolean style:string title:string variable:string

rails g scaffold Dictionary variable:string label:string comment:string

rails g scaffold LayerCategory name:string description:string

#model/casen.rb
  belongs_to :municipios, :class_name => 'Municipio', :primary_key => 'comuna', :foreign_key => 'comuna'
#model/municipio.rb
  has_many :casens, :class_name => 'Casen', :primary_key => 'comuna', :foreign_key => 'comuna'

load('./app/assets/create_municipios.rb')
load('./app/assets/create_dictionary.rb')

#PostgreSQL and PostGIS
gem 'pg'
gem 'activerecord-postgis-adapter'

common: &common
  adapter: postgis
  encoding: UTF-8
  pool: 5
  username: postgres
  password: admin
  host: localhost
  port: 5432
  schema_search_path: "public,postgis"
  script_dir: /Library/PostgreSQL/9.1/share/postgresql/contrib/postgis-1.5/
  postgis_extension: true
  template: template_postgis
development:
  <<: *common
  database: diaphanum_development

test:
  <<: *common
  database: diaphanum_test

production:
  <<: *common
  database: diaphanum_production

#load casen to model casen
require 'csv'
csv_text = File.read('app/assets/casen2011_csv_sample.csv')
csv = CSV.parse(csv_text, :headers => true, :col_sep => ";")
Casen.delete_all
csv.each do |row|
    a = row.to_hash
    a['year'] = 2011
    Casen.create!(a)
end

a = Dictionary.all
Layer.delete_all
b = [:edad_mean, :hombres_percentage, :mujeres_percentage, :soltero_percentage, :soltera_percentage, :casado_percentage, :casada_percentage, :emparejado_percentage, :emparejada_percentage, :nucleo_mean, :numper_mean, :e6a_mode, :e7_mode, :educ_mean, :educ_mode, :asiste_mean, :ytotaj_mean, :r11a_percentage, :r20_mean, :v1_mean, :v2_mean, :v5m_mean]
a.each do |row|
    if Municipio.column_names.include? row.variable.downcase
	    l = Layer.new
	    l.name = row.variable
	    l.variable = row.variable
	    l.opacity = 70
	    l.style = 'municipio_'+row.variable.downcase
	    l.title = row.label
	    l.minValue = Municipio.minimum(row.variable)
	    l.maxValue = Municipio.maximum(row.variable)
	    l.description = row.label
      l.layer_category_id = row.layer_category_id
	    l.show = b.include?row.variable.to_sym
      puts l.show
	    l.save
    end
end



#geoserver view
shp2pgsql -s 4326 -W latin1 cl_comunas_geo.shp cl_comunas_geo | psql -U postgres -d diaphanum_development
SELECT * FROM cl_comunas_geo
LEFT JOIN municipios ON cast(cl_comunas_geo.id_2002 as int)=municipios.comuna


otras variables para vivienda
tot_hogviv personas que viven en la vivienda
y29a Mantiene ahorro 1=si, 2= no, 9=NS/NR
y29b1 Ahorro en cuenta para la vivinda
y29b2 Ahorro en cta bancaria
y29b3 APV
y29b4 Ahorro en efectivo
y20a Prestamos bancarios
y30b Creditos en cajas de compensación
y30c Avance en efectivo casas comerciales
y30d Préstamo amigos y parientes
y30e Crédito con prestamistas o fiado
corte Pobres extremos = 1, Pobres no extremos = 2, 3 = no pobres
qaut Quintil de ingreso autónomo (1 al 5)
daut Decil de ingreso autónomo (1 al 10)
yataj Ingreso autónomo
yathaj Ingreso autónomo, hogar
ytotaj Ingreso total
ytothaj Ingreso total hogar
ymoneaj Ingreso monetario
yaimhaj Alquiler imputado, hogar
r10 Vehículo particular (1,2 o 9)
r11a Cantidad de vehículos para uso laboral
r11b Cantidad de vehículos para uso particular
r12 Seguro para su vehículo (excl SOAP) (1,2,9)
r13a Tiene Lavadora automática (1,2 o 9)
r13b Refrigerador
r13c Calefont
r13d Teléfono fijo
r13e TV de pago
r13f Computador
r14a Internet en la vivienda (B.Ancha fija contrato)
r14a Internet en la vivienda (B.Ancha fija prepago)
r14c Internet en la vivienda (B.Ancha móvil contrato)
r14d Internet en la vivienda (B.Ancha móvil prepago)
r14e Internet en la vivienda (smartphone)
r16 Donde utiliza internet (1= hogar, 2= trabajo, 3=estab.educacional, 4=infocentro, 5=cibercafe)
r17 Frecuencia de uso de internet (1=diariamente, 2=semanalmente, 3= mensual, 4=menos que mensual)
r18b Uso de internet para comunicacion escrita
r18c Uso de internet para comunicación por voz
r18d Uso de internet para entretenimiento
r18e Uso de internet para comercio electrónico
r18f Uso de internet para banca electrónica
r18g Uso de internet para capacitación
r18h Uso de internet para tramites públicos
r19 Teléfono móvil en uso (1=prepago, 2=contrato, 3=no, 9=NS/NR)
r20 Cuán satisfecho está con su vida (1 al 10)
v1 Cantidad de viviendas en el sitio (0 al 23)
v2 Metros cuadrados que tiene el sitio (1=menos de 100, 2= de 100 a 200, 3=200 a 300,4,5 y 9=NS)
v2frent Metros de frente
v2fo Metros de fondo
v3 Ocupacion del sitio (1=propio,2=pagandose,3=compartido pagado, 4=compartido pagandose, 5=arrendado con contrato, 6=arrendado sin contrato, 7=cedido por trabajo, 8=cedido por familiar, 9=usufructo, 10=ocupación irregular, 11=poseedor irregular)
v4 Alguien en el hogar es el responsable del sitio (1=si dueño, 2=si arriendo, 3=si cesión, 4=no)
v5 Metros cuadrados de la vivienda
v5m Metros cuadrados de la vivienda
 