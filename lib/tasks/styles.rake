require "net/http"

geoserver = 'http://admin:geoserver@localhost:8080'

task :styles => :environment do

def round_up(number)
	if number != 0
  		divisor = 10**Math.log10(number).to_i.floor
  		i = number.to_i / divisor
  		remainder = number % divisor
  		if remainder == 0
   		 i * divisor
  		else
    		(i + 1) * divisor
  		end
  	else
  		1	
	end
end

header =
'
<StyledLayerDescriptor version="1.0.0"
xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
xmlns="http://www.opengis.net/sld"
xmlns:ogc="http://www.opengis.net/ogc"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
'

random_colours_size = 16
random_colours = Paleta::Palette.generate(:type => :random, :size => random_colours_size)

layers_names = Municipio.column_names
#random_colours = Paleta::Palette.generate(:type => :random, :size => random_colours_size)
layers_names.each_with_index do |layer_name, index|
	
	if Municipio.columns_hash[layer_name] && Municipio.columns_hash[layer_name].type == :float
		min = Municipio.minimum(layer_name)
		max = Municipio.maximum(layer_name)
		#section_wide = round_up((max-min)/colour_sections)

		unless min.nil?

if min%1==0 and max%1==0
			colour_sections = (max-min).to_i+1
			section_wide = 1
		else
			colour_sections = 10
			section_wide = (max-min)/colour_sections
		end
		#puts min, max, section_wide, colour_sections
		rules = []
		#palette_shades = Paleta::Palette.generate_shades_from_color(random_colours[[*0..31].sample], 5)
		palette_shades = Paleta::Palette.generate(:type => :shades, :from => :color, :color=> random_colours[rand(random_colours_size)], :size => colour_sections)

		colour_sections.times do |section|

			cssFill = {:CssParameterFill => "#"+palette_shades[section].hex}
			cssStroke = {:CssParameterStroke => "#000000"}
			low = min+(section)*section_wide
			high = min+(section+1)*section_wide
			fill = {:Fill => cssFill}
			stroke = {:stroke => cssStroke}
			ogc_literal_low = {"ogc:Literal" => low}
			ogc_literal_high = {"ogc:Literal" => high}
			ogc_propertyName = {"ogc:PropertyName" => layer_name}
			ogc_is_less = {"ogc:PropertyIsGreaterThanOrEqualTo" => [ogc_propertyName, ogc_literal_low], "ogc:PropertyIsLessThan" => [ogc_propertyName, ogc_literal_high]}
			ogc_and = {"ogc:And" => ogc_is_less}
			rule = {"Rule"=>{"Name"=>"Rule " + section.to_s, "Title"=>"segmento " + (section+1).to_s + " de " + low.round(2).to_s + " a "+ high.round(2).to_s, "ogc:Filter"=> ogc_and, "PolygonSymbolizer"=> [fill, stroke]}}
			rules.push(rule)
		end
			name = {"Name"=> layer_name +" Style", "UserStyle" => {"Title"=> layer_name + " Style", "FeatureTypeStyle"=> rules}}
			xml = header + name.to_xml(:root=> "NamedLayer",:skip_types=>true,  :skip_instruct => true)
			xml = xml.gsub('<CssParameterFill>', '<CssParameter name="fill">')
			xml = xml.gsub('</CssParameterFill>', '</CssParameter>')
			xml = xml.gsub('<CssParameterStroke>', '<CssParameter name="stroke">')
			xml = xml.gsub('</CssParameterStroke>', '</CssParameter>')

			xml = xml.gsub('<FeatureTypeStyle>
      <FeatureTypeStyle>', '  <FeatureTypeStyle>')
			xml = xml.gsub('</FeatureTypeStyle>
      <FeatureTypeStyle>', '')
			xml = xml.gsub('</FeatureTypeStyle>
    </FeatureTypeStyle>', '</FeatureTypeStyle>')

			xml = xml.gsub('<PolygonSymbolizer>
            <PolygonSymbolizer>', '  <PolygonSymbolizer>')
			xml = xml.gsub('</PolygonSymbolizer>
            <PolygonSymbolizer>', '')
			xml = xml.gsub('</PolygonSymbolizer>
          </PolygonSymbolizer>', '</PolygonSymbolizer>')

			xml = xml.gsub('<ogc:PropertyIsLessThan>
                <ogc:PropertyIsLessThan>', '  <ogc:PropertyIsLessThan>')
			xml = xml.gsub('</ogc:PropertyIsLessThan>
                <ogc:PropertyIsLessThan>', '')
			xml = xml.gsub('</ogc:PropertyIsLessThan>
              </ogc:PropertyIsLessThan>', '</ogc:PropertyIsLessThan>')

			xml = xml.gsub('<ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsGreaterThanOrEqualTo>', '  <ogc:PropertyIsGreaterThanOrEqualTo>')
			xml = xml.gsub('</ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsGreaterThanOrEqualTo>', '')
			xml = xml.gsub('</ogc:PropertyIsGreaterThanOrEqualTo>
              </ogc:PropertyIsGreaterThanOrEqualTo>', '</ogc:PropertyIsGreaterThanOrEqualTo>')
			
			xml = xml+'</StyledLayerDescriptor>'
			
			puts layer_name

			requestAddress = geoserver+'/geoserver/rest/styles?name=municipio_'+layer_name

			#we delete the styles, this can be done only if there are no layers referencing the style
			#rm -rf /opt/opengeo/suite/data_dir/styles/municipio_*
			# deleterequestAddress = geoserver+'/geoserver/rest/styles/municipio_'+ layer_name +'?purge=true'
  			# RestClient.delete(deleterequestAddress)

  			RestClient.post(requestAddress, xml, :content_type => 'application/vnd.ogc.sld+xml')


			#File.open(layer_name + '.sld', 'w') { |file| file.write(xml) }
			
		end

		

	end
end


end 
