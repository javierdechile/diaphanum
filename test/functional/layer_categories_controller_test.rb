require 'test_helper'

class LayerCategoriesControllerTest < ActionController::TestCase
  setup do
    @layer_category = layer_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:layer_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create layer_category" do
    assert_difference('LayerCategory.count') do
      post :create, layer_category: { description: @layer_category.description, name: @layer_category.name }
    end

    assert_redirected_to layer_category_path(assigns(:layer_category))
  end

  test "should show layer_category" do
    get :show, id: @layer_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @layer_category
    assert_response :success
  end

  test "should update layer_category" do
    put :update, id: @layer_category, layer_category: { description: @layer_category.description, name: @layer_category.name }
    assert_redirected_to layer_category_path(assigns(:layer_category))
  end

  test "should destroy layer_category" do
    assert_difference('LayerCategory.count', -1) do
      delete :destroy, id: @layer_category
    end

    assert_redirected_to layer_categories_path
  end
end
