require 'test_helper'

class MunicipiosControllerTest < ActionController::TestCase
  setup do
    @municipio = municipios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:municipios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create municipio" do
    assert_difference('Municipio.count') do
      post :create, municipio: { asiste_mean: @municipio.asiste_mean, asiste_median: @municipio.asiste_median, asiste_standard_deviation: @municipio.asiste_standard_deviation, comuna: @municipio.comuna, e6a_mode: @municipio.e6a_mode, e7_mean: @municipio.e7_mean, e7_median: @municipio.e7_median, e7_mode: @municipio.e7_mode, e7_standard_deviation: @municipio.e7_standard_deviation, ecivil_mode: @municipio.ecivil_mode, edad_mean: @municipio.edad_mean, edad_median: @municipio.edad_median, edad_standard_deviation: @municipio.edad_standard_deviation, educ_mean: @municipio.educ_mean, educ_median: @municipio.educ_median, educ_mode: @municipio.educ_mode, educ_standard_deviation: @municipio.educ_standard_deviation, name: @municipio.name, nucleo_mean: @municipio.nucleo_mean, nucleo_median: @municipio.nucleo_median, nucleo_standard_deviation: @municipio.nucleo_standard_deviation, numper_mean: @municipio.numper_mean, numper_median: @municipio.numper_median, numper_standard_deviation: @municipio.numper_standard_deviation, pco2_mode: @municipio.pco2_mode, population: @municipio.population }
    end

    assert_redirected_to municipio_path(assigns(:municipio))
  end

  test "should show municipio" do
    get :show, id: @municipio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @municipio
    assert_response :success
  end

  test "should update municipio" do
    put :update, id: @municipio, municipio: { asiste_mean: @municipio.asiste_mean, asiste_median: @municipio.asiste_median, asiste_standard_deviation: @municipio.asiste_standard_deviation, comuna: @municipio.comuna, e6a_mode: @municipio.e6a_mode, e7_mean: @municipio.e7_mean, e7_median: @municipio.e7_median, e7_mode: @municipio.e7_mode, e7_standard_deviation: @municipio.e7_standard_deviation, ecivil_mode: @municipio.ecivil_mode, edad_mean: @municipio.edad_mean, edad_median: @municipio.edad_median, edad_standard_deviation: @municipio.edad_standard_deviation, educ_mean: @municipio.educ_mean, educ_median: @municipio.educ_median, educ_mode: @municipio.educ_mode, educ_standard_deviation: @municipio.educ_standard_deviation, name: @municipio.name, nucleo_mean: @municipio.nucleo_mean, nucleo_median: @municipio.nucleo_median, nucleo_standard_deviation: @municipio.nucleo_standard_deviation, numper_mean: @municipio.numper_mean, numper_median: @municipio.numper_median, numper_standard_deviation: @municipio.numper_standard_deviation, pco2_mode: @municipio.pco2_mode, population: @municipio.population }
    assert_redirected_to municipio_path(assigns(:municipio))
  end

  test "should destroy municipio" do
    assert_difference('Municipio.count', -1) do
      delete :destroy, id: @municipio
    end

    assert_redirected_to municipios_path
  end
end
