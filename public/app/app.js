Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'App',
    title: 'Diaphanum',

    appFolder: 'app',

    controllers: [
        'Maps', 
        'Layers'
    ],

    views: [
        'map.Panel',
        'layer.List',
        'result.Panel'
    ],


    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [

                {
                    xtype: 'resultPanel',
                    region: 'south',
                    collapsible: true,
                    split: true,
                    height: 250,
                    minHeight: 100,
                },
                

                {
                    xtype: 'panel',
                    region: 'east',
                    layout: 'border',
                    width: 400,
                    collapsible: true,
                    split: true,
                    title: 'Layers',
                    items: [
                            {
                                xtype: 'layerList',
                                region: 'center',
                                title: 'Filter',
                                width: 400, 
                            },
                            {
                                xtype: 'layerEdit',
                                region: 'south',
                                title: 'Edit Layer'
                            }]
                },
                {
                    xtype: 'mapPanel',
                    region: 'center',
                }
            ]
        });
    }
});