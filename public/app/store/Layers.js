Ext.define('App.store.Layers', {
    extend: 'Ext.data.Store',
    model: 'App.model.Layer',
    autoLoad: true,

    filters: [{
         property: 'show',
         value: true
     }],

    listeners: {
         load: function(store) {

            for (var i = store.data.items.length - 1; i >= 0; i--) {
                var layer = store.data.items[i].data;

                if (layer.show) {

                    wmsLayer = new OpenLayers.Layer.WMS(
                    layer.name, "/geoserver/opengeo/wms",
                    {
                      LAYERS: 'opengeo:municipios',
                      format: 'image/png8',
                      tiled: true,
                      transparent: true,
                      STYLES: layer.style.toLowerCase(), //TODO: fix case from data model. not here
                    },
                    {
                      buffer: 0,
                      displayOutsideMaxExtent: false,
                      isBaseLayer: false,
                      opacity: 0.5,
                      transitionEffect: 'resize',
                      yx : {'EPSG:900913' : true}
                    }
                );    

                App.model.Map.addLayer(wmsLayer, layer.name);

                };

            App.model.Map.hideAllLayers();

            };
      }
    },

    proxy: {
        type: 'rest',
        url: '/layers.json',
                
        reader: {
            type: 'json'
        },
        writer:{
            type: 'json'
        }
    }
});