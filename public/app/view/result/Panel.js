Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', './resources/extjs/ux');
Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.ux.grid.FiltersFeature',
    'Ext.toolbar.Paging',
    'Ext.ux.ajax.JsonSimlet',
    'Ext.ux.ajax.SimManager'
]);

Ext.define('App.view.result.Panel' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.resultPanel',
    title: 'Results',
    id: 'resultPanel',
    maximizable: true,
    layout:'fit',

    reloadLayers: function(selectedLayer){
        console.log('reloading layers');

        //filters

        var layerEdit = Ext.getCmp('layerEdit');

        wmsLayer = App.model.Map.getLayer(selectedLayer);

        var sliderLowValue = layerEdit.filterSlider.thumbs[0].value;
        var sliderHighValue = layerEdit.filterSlider.thumbs[1].value;
        var filter = layerEdit.layer.variable.toLowerCase()
        wmsLayer.mergeNewParams({'CQL_FILTER': filter+" BETWEEN " + sliderLowValue + " AND " + sliderHighValue}); 
        wmsLayer.redraw();
        wmsLayer.sliderLowValue = sliderLowValue;
        wmsLayer.sliderHighValue = sliderHighValue;

        var resultPanel = this;

        //update columns to show
        columns = this.columns;
        columns[0].setVisible(true) //set visible name column
        selectedLayerColumn = $.grep(columns, function(e){ return e.dataIndex == selectedLayer; });

        if (selectedLayerColumn[0].isHidden())  selectedLayerColumn[0].setVisible(true);

        Ext.Ajax.request({
                url: 'filter.json',
                method: 'GET',
                type: 'rest',
                params: {
                    filters: [filter],
                    lowvalues: [sliderLowValue],
                    highvalues: [sliderHighValue]
                },
                success: function(response){
                    var text = response.responseText;
                    resultPanel.setTitle('Resultados para filtro aplicado')

                    console.log(resultPanel);
                    store = resultPanel.getStore();
                    store.loadData(Ext.decode(text));
                }
            });


            
    },
   

    initComponent: function() {


        var filters = {
            ftype: 'filters',
            local: true,
            encode: false,
            filters: [{
                type: 'numeric',
            }]
            // Filters are most naturally placed in the column definition, but can also be
            // added here.
        };



        this.columns = [
            {header: 'Name', dataIndex: 'name', flex: 2, filterable: true, filter: {type: 'string'} ,summaryType: 'count', 
                summaryRenderer: function(){
                    return '<b>TOTAL</b>';
                }
            },

                {
                    header: 'Edad (promedio)', dataIndex: 'edad_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Edad (media)', dataIndex: 'edad_median', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Edad (desv)', dataIndex: 'edad_standard_deviation', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Estado Civil (moda)', dataIndex: 'ecivil_mode', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Hombres (%)', dataIndex: 'hombre_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Mujeres (%)', dataIndex: 'mujer_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Solteros (%)', dataIndex: 'soltero_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Solteras (%)', dataIndex: 'soltera_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Casados (%)', dataIndex: 'casado_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Casadas (%)', dataIndex: 'casada_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Hombres conviviendo (%)', dataIndex: 'emparejado_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Mujeres conviviendo (%)', dataIndex: 'emparejada_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Nucleos familiares (promedio)', dataIndex: 'nucleo_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Núcleos familiares (media)', dataIndex: 'nucleo_median', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Núcleos familiaress (desv)', dataIndex: 'nucleo_standard_deviation', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Jefé núcleo', dataIndex: 'pco2_mode', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Personas (promedio)', dataIndex: 'numper_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Personas (media)', dataIndex: 'numper_median', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Personas (desv)', dataIndex: 'numper_standard_deviation', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Nivel educacional (moda)', dataIndex: 'e6a_mode', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Duración Carrera (promedio)', dataIndex: 'e7_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Duración Carrera (media)', dataIndex: 'e7_median', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Duración Carrera (desv)', dataIndex: 'e7_standard_deviation', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Duración Carrera (moda)', dataIndex: 'e7_mode', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Nivel Educ. (promedio)', dataIndex: 'educ_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Nivel Educ. (media)', dataIndex: 'educ_median', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Nivel Educ. (desv)', dataIndex: 'educ_standard_deviation', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Nivel Educ. (moda)', dataIndex: 'educ_mode', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'asiste_mean', dataIndex: 'asiste_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'asiste_median', dataIndex: 'asiste_median', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'asiste_standard_deviation', dataIndex: 'asiste_standard_deviation', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'tot_hogviv_mean', dataIndex: 'tot_hogviv_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Mantiene Ahorro (%)', dataIndex: 'y29a_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Cuenta para vivienda (%)', dataIndex: 'y29b1_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Cuenta bancaria (%)', dataIndex: 'y29b2_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'APV (%)', dataIndex: 'y29b3_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Ahorro efectivo (%)', dataIndex: 'y29b4_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Prestamos bancarios (%)', dataIndex: 'y20a_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Creditos caja compensación (%)', dataIndex: 'y30b_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Avance efec. casas comerciales (%)', dataIndex: 'y30c_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Prestamo amigos o familiares', dataIndex: 'y30d_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Credito a prestamistas (%)', dataIndex: 'y30e_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'corte_percentage', dataIndex: 'corte_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Quintil ingreso autónomo (promedio)', dataIndex: 'qaut_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Decil ingreso autónomo (promedio)', dataIndex: 'daut_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Ingreso autónomo', dataIndex: 'yataj_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Ingreso autónomo (hogar)', dataIndex: 'yathaj_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Ingreso total', dataIndex: 'ytotaj_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Ingreso total hogar', dataIndex: 'ytothaj_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Ingreso monetario', dataIndex: 'ymoneaj_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Alquiler imputado', dataIndex: 'yaimhaj_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Posee vehículo particular', dataIndex: 'r10_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Posee solo un vehículo trabajo (%)', dataIndex: 'r11a_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Posee solo un vehiculo particular', dataIndex: 'r11b_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Seguro vehicular (%)', dataIndex: 'r12_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Lavadora automática (%)', dataIndex: 'r13a_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Refrigerador (%)', dataIndex: 'r13b_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Calefont (%)', dataIndex: 'r13c_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Teléfono fijo (%)', dataIndex: 'r13d_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'TV pago (%)', dataIndex: 'r13e_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Computador (%)', dataIndex: 'r13f_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'B.A. Fija contrato (%)', dataIndex: 'r14a_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'B.A. Fija prepago (%)', dataIndex: 'r14b_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'B.A. movil contrato (%)', dataIndex: 'r14c_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'B.A. móvil prepago (%)', dataIndex: 'r14d_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Smartphone (%)', dataIndex: 'r14e_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Utiliza internet hogar (%)', dataIndex: 'r16_mode', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet diariamente (%)', dataIndex: 'r17_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet comunicación escrita (%)', dataIndex: 'r18b_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet comunicación por voz (%)', dataIndex: 'r18c_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet para entretenimiento (%)', dataIndex: 'r18d_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet para comercio elect. (%)', dataIndex: 'r18e_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa banca electrónica (%)', dataIndex: 'r18f_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet para capacitación (%)', dataIndex: 'r18g_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa internet para trámites', dataIndex: 'r18h_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Usa un teléfono prepago', dataIndex: 'r19_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Satisfacción con la vida (promedio 1 al 10)', dataIndex: 'r20_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'Viviendas en sitio (promedio)', dataIndex: 'v1_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'M2 del sitio (promedio)', dataIndex: 'v2_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'v2frent_mean', dataIndex: 'v2frent_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'v2fo_mean', dataIndex: 'v2fo_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'v3_percentage', dataIndex: 'v3_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'v4_percentage', dataIndex: 'v4_percentage', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'v5_mean', dataIndex: 'v5_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                },{
                    header: 'v5m_mean', dataIndex: 'v5m_mean', flex:1, summaryType: 'average', filterable:true, hidden: true
                }


        ];

Ext.define('resultModel', {
            extend: 'Ext.data.Model',
            fields: [

                {
                    name: 'comuna',
                    type: 'integer'
                },{
                    name: 'name',
                    type: 'string'
                },{
                    name: 'edad_mean',
                    
                    type: 'float'
                },{
                    name: 'edad_median',
                    
                    type: 'float'
                },{
                    name: 'edad_standard_deviation',
                    
                    type: 'float'
                },{
                    name: 'ecivil_mode',
                    
                    type: 'float'
                },{
                    name: 'hombre_percentage',
                    
                    type: 'float'
                },{
                    name: 'mujer_percentage',
                    
                    type: 'float'
                },{
                    name: 'soltero_percentage',
                    
                    type: 'float'
                },{
                    name: 'soltera_percentage',
                    
                    type: 'float'
                },{
                    name: 'casado_percentage',
                    
                    type: 'float'
                },{
                    name: 'casada_percentage',
                    
                    type: 'float'
                },{
                    name: 'emparejado_percentage',
                    
                    type: 'float'
                },{
                    name: 'emparejada_percentage',
                    
                    type: 'float'
                },{
                    name: 'nucleo_mean',
                    
                    type: 'float'
                },{
                    name: 'nucleo_median',
                    
                    type: 'float'
                },{
                    name: 'nucleo_standard_deviation',
                    
                    type: 'float'
                },{
                    name: 'pco2_mode',
                    
                    type: 'float'
                },{
                    name: 'numper_mean',
                    
                    type: 'float'
                },{
                    name: 'numper_median',
                    
                    type: 'float'
                },{
                    name: 'numper_standard_deviation',
                    
                    type: 'float'
                },{
                    name: 'e6a_mode',
                    
                    type: 'float'
                },{
                    name: 'e7_mean',
                    
                    type: 'float'
                },{
                    name: 'e7_median',
                    
                    type: 'float'
                },{
                    name: 'e7_standard_deviation',
                    
                    type: 'float'
                },{
                    name: 'e7_mode',
                    
                    type: 'float'
                },{
                    name: 'educ_mean',
                    
                    type: 'float'
                },{
                    name: 'educ_median',
                    
                    type: 'float'
                },{
                    name: 'educ_standard_deviation',
                    
                    type: 'float'
                },{
                    name: 'educ_mode',
                    
                    type: 'float'
                },{
                    name: 'asiste_mean',
                    
                    type: 'float'
                },{
                    name: 'asiste_median',
                    
                    type: 'float'
                },{
                    name: 'asiste_standard_deviation',
                    
                    type: 'float'
                },{
                    name: 'tot_hogviv_mean',
                    
                    type: 'float'
                },{
                    name: 'y29a_percentage',
                    
                    type: 'float'
                },{
                    name: 'y29b1_percentage',
                    
                    type: 'float'
                },{
                    name: 'y29b2_percentage',
                    
                    type: 'float'
                },{
                    name: 'y29b3_percentage',
                    
                    type: 'float'
                },{
                    name: 'y29b4_percentage',
                    
                    type: 'float'
                },{
                    name: 'y20a_percentage',
                    
                    type: 'float'
                },{
                    name: 'y30b_percentage',
                    
                    type: 'float'
                },{
                    name: 'y30c_percentage',
                    
                    type: 'float'
                },{
                    name: 'y30d_percentage',
                    
                    type: 'float'
                },{
                    name: 'y30e_percentage',
                    
                    type: 'float'
                },{
                    name: 'corte_percentage',
                    
                    type: 'float'
                },{
                    name: 'qaut_mean',
                    
                    type: 'float'
                },{
                    name: 'daut_mean',
                    
                    type: 'float'
                },{
                    name: 'yataj_mean',
                    
                    type: 'float'
                },{
                    name: 'yathaj_mean',
                    
                    type: 'float'
                },{
                    name: 'ytotaj_mean',
                    
                    type: 'float'
                },{
                    name: 'ytothaj_mean',
                    
                    type: 'float'
                },{
                    name: 'ymoneaj_mean',
                    
                    type: 'float'
                },{
                    name: 'yaimhaj_mean',
                    
                    type: 'float'
                },{
                    name: 'r10_percentage',
                    
                    type: 'float'
                },{
                    name: 'r11a_percentage',
                    
                    type: 'float'
                },{
                    name: 'r11b_percentage',
                    
                    type: 'float'
                },{
                    name: 'r12_percentage',
                    
                    type: 'float'
                },{
                    name: 'r13a_percentage',
                    
                    type: 'float'
                },{
                    name: 'r13b_percentage',
                    
                    type: 'float'
                },{
                    name: 'r13c_percentage',
                    
                    type: 'float'
                },{
                    name: 'r13d_percentage',
                    
                    type: 'float'
                },{
                    name: 'r13e_percentage',
                    
                    type: 'float'
                },{
                    name: 'r13f_percentage',
                    
                    type: 'float'
                },{
                    name: 'r14a_percentage',
                    
                    type: 'float'
                },{
                    name: 'r14b_percentage',
                    
                    type: 'float'
                },{
                    name: 'r14c_percentage',
                    
                    type: 'float'
                },{
                    name: 'r14d_percentage',
                    
                    type: 'float'
                },{
                    name: 'r14e_percentage',
                    
                    type: 'float'
                },{
                    name: 'r16_mode',
                    
                    type: 'float'
                },{
                    name: 'r17_mean',
                    
                    type: 'float'
                },{
                    name: 'r18b_percentage',
                    
                    type: 'float'
                },{
                    name: 'r18c_percentage',
                    
                    type: 'float'
                },{
                    name: 'r18d_percentage',
                    
                    type: 'float'
                },{
                    name: 'r18e_percentage',
                    
                    type: 'float'
                },{
                    name: 'r18f_percentage',
                    
                    type: 'float'
                },{
                    name: 'r18g_percentage',
                    
                    type: 'float'
                },{
                    name: 'r18h_percentage',
                    
                    type: 'float'
                },{
                    name: 'r19_percentage',
                    
                    type: 'float'
                },{
                    name: 'r20_mean',
                    
                    type: 'float'
                },{
                    name: 'v1_mean',
                    
                    type: 'float'
                },{
                    name: 'v2_mean',
                    
                    type: 'float'
                },{
                    name: 'v2frent_mean',
                    
                    type: 'float'
                },{
                    name: 'v2fo_mean',
                    
                    type: 'float'
                },{
                    name: 'v3_percentage',
                    
                    type: 'float'
                },{
                    name: 'v4_percentage',
                    
                    type: 'float'
                },{
                    name: 'v5_mean',
                    
                    type: 'float'
                },{
                    name: 'v5m_mean',

                    type: 'float'
                }
            ]
        });

        var store = Ext.create('Ext.data.Store', {
            model: 'resultModel',
        });

        this.store = store;

        this.features = [
        filters,
        {
            ftype: 'summary'
        }];


        // var chart = Ext.create('Ext.chart.Chart', {
        //        renderTo: Ext.getBody(),
        //        width: 400,
        //        height: 300,
        //        store: store,

        //        axes: [
        //             {
        //                 title: 'Temperature',
        //                 type: 'Numeric',
        //                 position: 'left',
        //                 fields: ['temperature'],
        //                 minimum: 0,
        //                 maximum: 100
        //             },
        //             {
        //                 title: 'Time',
        //                 type: 'Time',
        //                 position: 'bottom',
        //                 fields: ['date'],
        //                 dateFormat: 'ga'
        //             }
        //         ],

        //         series: [
        //             {
        //                 type: 'line',
        //                 xField: 'date',
        //                 yField: 'temperature'
        //             }
        //         ]
        // });

        // this.items = [chart]

        this.callParent(arguments);

    },

});