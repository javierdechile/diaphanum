Ext.define('App.view.chart.Panel' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.chartPanel',
    title: 'Charts',
    id: 'chartPanel',
    maximizable: true,
    layout:'fit',


    // reloadLayers: function(selectedLayer){
    //     console.log('reloading layers');



    //     //filters

    //     var layerEdit = Ext.getCmp('layerEdit');

    //     wmsLayer = App.model.Map.getLayer(selectedLayer);

    //     var sliderLowValue = layerEdit.filterSlider.thumbs[0].value;
    //     var sliderHighValue = layerEdit.filterSlider.thumbs[1].value;
    //     var filter = layerEdit.layer.variable.toLowerCase()
    //     wmsLayer.mergeNewParams({'CQL_FILTER': filter+" BETWEEN " + sliderLowValue + " AND " + sliderHighValue}); 
    //     wmsLayer.redraw();
    //     wmsLayer.sliderLowValue = sliderLowValue;
    //     wmsLayer.sliderHighValue = sliderHighValue;

    //     var resultPanel = this;

    //     Ext.Ajax.request({
    //             url: 'filter.json',
    //             method: 'GET',
    //             type: 'rest',
    //             params: {
    //                 filters: [filter],
    //                 lowvalues: [sliderLowValue],
    //                 highvalues: [sliderHighValue]
    //             },
    //             success: function(response){
    //                 var text = response.responseText;
    //                 resultPanel.setTitle('Resultados para filtro aplicado')
    //                 resultPanel.store.loadData(Ext.decode(text));
    //             }
    //         });

    //     //update columns to show
    //     var columns = this.columns;

    //         for (var i = columns.length - 1; i >= 0; i--) {
    //             if (columns[i].dataIndex == selectedLayer || columns[i].dataIndex == 'name' ) {
    //                 columns[i].setVisible(true);
    //             } 
    //             else{
    //                 columns[i].setVisible(false);
    //             };
    //         };




            
    // },
   

    initComponent: function() {

        //layers = var layerEdit = Ext.getCmp('layerList'); // to do... get the layer list

        this.columns = [
            {header: 'Name', dataIndex: 'name', flex: 2, summaryType: 'count', 
                summaryRenderer: function(){
                    return '<b>TOTAL</b>';
                }
            },

                {
                    header: 'Edad (promedio)', dataIndex: 'edad_mean', flex:1, summaryType: 'average'
                },{
                    header: 'Edad (media)', dataIndex: 'edad_median', flex:1, summaryType: 'average'
                },{
                    header: 'Edad (desv)', dataIndex: 'edad_standard_deviation', flex:1, summaryType: 'average'
                },{
                    header: 'Estado Civil (moda)', dataIndex: 'ecivil_mode', flex:1, summaryType: 'average'
                },{
                    header: 'Hombres (%)', dataIndex: 'hombre_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Mujeres (%)', dataIndex: 'mujer_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Solteros (%)', dataIndex: 'soltero_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Solteras (%)', dataIndex: 'soltera_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Casados (%)', dataIndex: 'casado_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Casadas (%)', dataIndex: 'casada_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Hombres conviviendo (%)', dataIndex: 'emparejado_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Mujeres conviviendo (%)', dataIndex: 'emparejada_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'Nucleos familiares (promedio)', dataIndex: 'nucleo_mean', flex:1, summaryType: 'average'
                },{
                    header: 'Núcleos familiares (media)', dataIndex: 'nucleo_median', flex:1, summaryType: 'average'
                },{
                    header: 'Núcleos familiaress (desv)', dataIndex: 'nucleo_standard_deviation', flex:1, summaryType: 'average'
                },{
                    header: 'pco2_mode', dataIndex: 'pco2_mode', flex:1, summaryType: 'average'
                },{
                    header: 'numper_mean', dataIndex: 'numper_mean', flex:1, summaryType: 'average'
                },{
                    header: 'numper_median', dataIndex: 'numper_median', flex:1, summaryType: 'average'
                },{
                    header: 'numper_standard_deviation', dataIndex: 'numper_standard_deviation', flex:1, summaryType: 'average'
                },{
                    header: 'e6a_mode', dataIndex: 'e6a_mode', flex:1, summaryType: 'average'
                },{
                    header: 'e7_mean', dataIndex: 'e7_mean', flex:1, summaryType: 'average'
                },{
                    header: 'e7_median', dataIndex: 'e7_median', flex:1, summaryType: 'average'
                },{
                    header: 'e7_standard_deviation', dataIndex: 'e7_standard_deviation', flex:1, summaryType: 'average'
                },{
                    header: 'e7_mode', dataIndex: 'e7_mode', flex:1, summaryType: 'average'
                },{
                    header: 'educ_mean', dataIndex: 'educ_mean', flex:1, summaryType: 'average'
                },{
                    header: 'educ_median', dataIndex: 'educ_median', flex:1, summaryType: 'average'
                },{
                    header: 'educ_standard_deviation', dataIndex: 'educ_standard_deviation', flex:1, summaryType: 'average'
                },{
                    header: 'educ_mode', dataIndex: 'educ_mode', flex:1, summaryType: 'average'
                },{
                    header: 'asiste_mean', dataIndex: 'asiste_mean', flex:1, summaryType: 'average'
                },{
                    header: 'asiste_median', dataIndex: 'asiste_median', flex:1, summaryType: 'average'
                },{
                    header: 'asiste_standard_deviation', dataIndex: 'asiste_standard_deviation', flex:1, summaryType: 'average'
                },{
                    header: 'tot_hogviv_mean', dataIndex: 'tot_hogviv_mean', flex:1, summaryType: 'average'
                },{
                    header: 'y29a_percentage', dataIndex: 'y29a_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y29b1_percentage', dataIndex: 'y29b1_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y29b2_percentage', dataIndex: 'y29b2_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y29b3_percentage', dataIndex: 'y29b3_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y29b4_percentage', dataIndex: 'y29b4_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y20a_percentage', dataIndex: 'y20a_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y30b_percentage', dataIndex: 'y30b_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y30c_percentage', dataIndex: 'y30c_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y30d_percentage', dataIndex: 'y30d_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'y30e_percentage', dataIndex: 'y30e_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'corte_percentage', dataIndex: 'corte_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'qaut_mean', dataIndex: 'qaut_mean', flex:1, summaryType: 'average'
                },{
                    header: 'daut_mean', dataIndex: 'daut_mean', flex:1, summaryType: 'average'
                },{
                    header: 'yataj_mean', dataIndex: 'yataj_mean', flex:1, summaryType: 'average'
                },{
                    header: 'yathaj_mean', dataIndex: 'yathaj_mean', flex:1, summaryType: 'average'
                },{
                    header: 'ytotaj_mean', dataIndex: 'ytotaj_mean', flex:1, summaryType: 'average'
                },{
                    header: 'ytothaj_mean', dataIndex: 'ytothaj_mean', flex:1, summaryType: 'average'
                },{
                    header: 'ymoneaj_mean', dataIndex: 'ymoneaj_mean', flex:1, summaryType: 'average'
                },{
                    header: 'yaimhaj_mean', dataIndex: 'yaimhaj_mean', flex:1, summaryType: 'average'
                },{
                    header: 'r10_percentage', dataIndex: 'r10_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r11a_percentage', dataIndex: 'r11a_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r11b_percentage', dataIndex: 'r11b_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r12_percentage', dataIndex: 'r12_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r13a_percentage', dataIndex: 'r13a_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r13b_percentage', dataIndex: 'r13b_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r13c_percentage', dataIndex: 'r13c_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r13d_percentage', dataIndex: 'r13d_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r13e_percentage', dataIndex: 'r13e_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r13f_percentage', dataIndex: 'r13f_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r14a_percentage', dataIndex: 'r14a_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r14b_percentage', dataIndex: 'r14b_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r14c_percentage', dataIndex: 'r14c_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r14d_percentage', dataIndex: 'r14d_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r14e_percentage', dataIndex: 'r14e_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r16_mode', dataIndex: 'r16_mode', flex:1, summaryType: 'average'
                },{
                    header: 'r17_mean', dataIndex: 'r17_mean', flex:1, summaryType: 'average'
                },{
                    header: 'r18b_percentage', dataIndex: 'r18b_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r18c_percentage', dataIndex: 'r18c_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r18d_percentage', dataIndex: 'r18d_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r18e_percentage', dataIndex: 'r18e_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r18f_percentage', dataIndex: 'r18f_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r18g_percentage', dataIndex: 'r18g_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r18h_percentage', dataIndex: 'r18h_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r19_percentage', dataIndex: 'r19_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'r20_mean', dataIndex: 'r20_mean', flex:1, summaryType: 'average'
                },{
                    header: 'v1_mean', dataIndex: 'v1_mean', flex:1, summaryType: 'average'
                },{
                    header: 'v2_mean', dataIndex: 'v2_mean', flex:1, summaryType: 'average'
                },{
                    header: 'v2frent_mean', dataIndex: 'v2frent_mean', flex:1, summaryType: 'average'
                },{
                    header: 'v2fo_mean', dataIndex: 'v2fo_mean', flex:1, summaryType: 'average'
                },{
                    header: 'v3_percentage', dataIndex: 'v3_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'v4_percentage', dataIndex: 'v4_percentage', flex:1, summaryType: 'average'
                },{
                    header: 'v5_mean', dataIndex: 'v5_mean', flex:1, summaryType: 'average'
                },{
                    header: 'v5m_mean', dataIndex: 'v5m_mean', flex:1, summaryType: 'average'
                }


        ];

        Ext.define('resultModel', {
            extend: 'Ext.data.Model',
            fields: [

                {
                    name: 'comuna', type: 'integer'
                },{
                    name: 'name', type: 'string'
                },{
                    name: 'edad_mean', type: 'float'
                },{
                    name: 'edad_median', type: 'float'
                },{
                    name: 'edad_standard_deviation', type: 'float'
                },{
                    name: 'ecivil_mode', type: 'float'
                },{
                    name: 'hombre_percentage', type: 'float'
                },{
                    name: 'mujer_percentage', type: 'float'
                },{
                    name: 'soltero_percentage', type: 'float'
                },{
                    name: 'soltera_percentage', type: 'float'
                },{
                    name: 'casado_percentage', type: 'float'
                },{
                    name: 'casada_percentage', type: 'float'
                },{
                    name: 'emparejado_percentage', type: 'float'
                },{
                    name: 'emparejada_percentage', type: 'float'
                },{
                    name: 'nucleo_mean', type: 'float'
                },{
                    name: 'nucleo_median', type: 'float'
                },{
                    name: 'nucleo_standard_deviation', type: 'float'
                },{
                    name: 'pco2_mode', type: 'float'
                },{
                    name: 'numper_mean', type: 'float'
                },{
                    name: 'numper_median', type: 'float'
                },{
                    name: 'numper_standard_deviation', type: 'float'
                },{
                    name: 'e6a_mode', type: 'float'
                },{
                    name: 'e7_mean', type: 'float'
                },{
                    name: 'e7_median', type: 'float'
                },{
                    name: 'e7_standard_deviation', type: 'float'
                },{
                    name: 'e7_mode', type: 'float'
                },{
                    name: 'educ_mean', type: 'float'
                },{
                    name: 'educ_median', type: 'float'
                },{
                    name: 'educ_standard_deviation', type: 'float'
                },{
                    name: 'educ_mode', type: 'float'
                },{
                    name: 'asiste_mean', type: 'float'
                },{
                    name: 'asiste_median', type: 'float'
                },{
                    name: 'asiste_standard_deviation', type: 'float'
                },{
                    name: 'tot_hogviv_mean', type: 'float'
                },{
                    name: 'y29a_percentage', type: 'float'
                },{
                    name: 'y29b1_percentage', type: 'float'
                },{
                    name: 'y29b2_percentage', type: 'float'
                },{
                    name: 'y29b3_percentage', type: 'float'
                },{
                    name: 'y29b4_percentage', type: 'float'
                },{
                    name: 'y20a_percentage', type: 'float'
                },{
                    name: 'y30b_percentage', type: 'float'
                },{
                    name: 'y30c_percentage', type: 'float'
                },{
                    name: 'y30d_percentage', type: 'float'
                },{
                    name: 'y30e_percentage', type: 'float'
                },{
                    name: 'corte_percentage', type: 'float'
                },{
                    name: 'qaut_mean', type: 'float'
                },{
                    name: 'daut_mean', type: 'float'
                },{
                    name: 'yataj_mean', type: 'float'
                },{
                    name: 'yathaj_mean', type: 'float'
                },{
                    name: 'ytotaj_mean', type: 'float'
                },{
                    name: 'ytothaj_mean', type: 'float'
                },{
                    name: 'ymoneaj_mean', type: 'float'
                },{
                    name: 'yaimhaj_mean', type: 'float'
                },{
                    name: 'r10_percentage', type: 'float'
                },{
                    name: 'r11a_percentage', type: 'float'
                },{
                    name: 'r11b_percentage', type: 'float'
                },{
                    name: 'r12_percentage', type: 'float'
                },{
                    name: 'r13a_percentage', type: 'float'
                },{
                    name: 'r13b_percentage', type: 'float'
                },{
                    name: 'r13c_percentage', type: 'float'
                },{
                    name: 'r13d_percentage', type: 'float'
                },{
                    name: 'r13e_percentage', type: 'float'
                },{
                    name: 'r13f_percentage', type: 'float'
                },{
                    name: 'r14a_percentage', type: 'float'
                },{
                    name: 'r14b_percentage', type: 'float'
                },{
                    name: 'r14c_percentage', type: 'float'
                },{
                    name: 'r14d_percentage', type: 'float'
                },{
                    name: 'r14e_percentage', type: 'float'
                },{
                    name: 'r16_mode', type: 'float'
                },{
                    name: 'r17_mean', type: 'float'
                },{
                    name: 'r18b_percentage', type: 'float'
                },{
                    name: 'r18c_percentage', type: 'float'
                },{
                    name: 'r18d_percentage', type: 'float'
                },{
                    name: 'r18e_percentage', type: 'float'
                },{
                    name: 'r18f_percentage', type: 'float'
                },{
                    name: 'r18g_percentage', type: 'float'
                },{
                    name: 'r18h_percentage', type: 'float'
                },{
                    name: 'r19_percentage', type: 'float'
                },{
                    name: 'r20_mean', type: 'float'
                },{
                    name: 'v1_mean', type: 'float'
                },{
                    name: 'v2_mean', type: 'float'
                },{
                    name: 'v2frent_mean', type: 'float'
                },{
                    name: 'v2fo_mean', type: 'float'
                },{
                    name: 'v3_percentage', type: 'float'
                },{
                    name: 'v4_percentage', type: 'float'
                },{
                    name: 'v5_mean', type: 'float'
                },{
                    name: 'v5m_mean', type: 'float'
                }
            ]
        });

        var store = Ext.create('Ext.data.Store', {
            model: 'resultModel',
        });

        this.store = store;


        var chart = Ext.create('Ext.chart.Chart', {
               renderTo: Ext.getBody(),
               width: 400,
               height: 300,
               store: store,

               axes: [
                    {
                        title: 'Temperature',
                        type: 'Numeric',
                        position: 'left',
                        fields: ['temperature'],
                        minimum: 0,
                        maximum: 100
                    },
                    {
                        title: 'Time',
                        type: 'Time',
                        position: 'bottom',
                        fields: ['date'],
                        dateFormat: 'ga'
                    }
                ],

                series: [
                    {
                        type: 'line',
                        xField: 'date',
                        yField: 'temperature'
                    }
                ]
        });

        this.items = [chart]

        this.callParent(arguments);

    },

});