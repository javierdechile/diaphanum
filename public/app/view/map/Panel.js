Ext.define('App.view.map.Panel' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.mapPanel',
    title: 'Map',
    id: 'mapPanel',
    layout:'fit',

    initComponent: function() {

            var getResults = function(){

                    vector = App.model.Map.getLayer('draft').features[0];

                    var geographic = new OpenLayers.Projection("EPSG:4269");
                    var wgs84 = new OpenLayers.Projection("EPSG:4326");
                    var mercator = new OpenLayers.Projection("EPSG:900913");

                    //we clone the vector, so the transformation don't affect the shown vector
                    newgeo = vector.clone()
                    newgeo.geometry.transform(mercator, wgs84)
                    wkt = new OpenLayers.Format.WKT();
                    var vectorAsWKT = wkt.write(newgeo);

            }

            this.getResults = getResults;


            var radioSlider = Ext.create('Ext.slider.Single', {
                minValue    :0.5,
                maxValue    :30,
                value   : 3, 
                width   : 150,
                name    : 'radio',
                textField: 'Radio (Mi):'
            });

            var radioField = Ext.create('Ext.form.field.Number',{
                width: 50,
                name: 'radioField',
                minValue: radioSlider.minValue, //prevents negative numbers
                maxValue: radioSlider.maxValue,
                value: radioSlider.value
            });


            mapLegend = Ext.create('Ext.Window', {
                title: 'Map Legend',
                closable: false,
                collapsible: true,
                id: 'mapLegend',
                width: 265,
                height: 215,
                html: 'Please select a Layer',
                x: 10,
                y: 200,
                headerPosition: 'left',
                items: {
                    border: false
                }
            }).show();

            radioSlider.on('change', function(e, value){

                radioField.setValue(value);
                var draftLayer = App.model.Map.getLayer('draft');
                oldgeom = draftLayer.features[0].geometry;

                draftLayer.removeAllFeatures();
                var mycircle = OpenLayers.Geometry.Polygon.createRegularPolygon(oldgeom.getCentroid(), value*1609, 100, 0);
                var featurecircle = new OpenLayers.Feature.Vector(mycircle);
                draftLayer.addFeatures([featurecircle]);

                App.model.Map.getMap().zoomToExtent(draftLayer.getDataExtent());

                getResults();

            });

            radioField.on('change', function(e,value){
                radioSlider.setValue(value);
            })


            var toolbar = Ext.create('Ext.toolbar.Toolbar', {
                width   : 500,
                items: [
                      '->', // same as { xtype: 'tbfill' }
                    'Search:',
                    {
                        id       : 'searchTextField',
                        xtype    : 'textfield',
                        name     : 'field1',
                        width: "250", 
                        emptyText: 'Enter Address'
                    },
                    // begin using the right-justified button container
                    'Radio (miles):',
                    radioSlider,
                    radioField                ]
            });
    
        //this.dockedItems = [toolbar];

        this.callParent(arguments);

    },

});