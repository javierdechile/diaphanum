Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.selection.CheckboxModel'
]);

Ext.define('App.view.layer.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.layerList',
    selType: 'checkboxmodel',
    store: 'Layers',
    header: false,

    initComponent: function() {

        var sm = Ext.create('Ext.selection.CheckboxModel', {
            listeners: {
                selectionchange: function(sm, selections){

                    App.model.Map.hideAllLayers();

                    for(var index in selections){
                        var selection = selections[index];
                        if(selection.data != undefined && !selection.data.isbaselayer){
                            layer = selection.data
                            wmsLayer = App.model.Map.getLayer(layer.name)
                            wmsLayer.setVisibility(true);
                            legendWindow = Ext.getCmp('mapLegend');
                            var img = document.createElement("IMG");
                            img.src = '/geoserver/opengeo/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=30&HEIGHT=20&STRICT=false&style='+selection.data.style.toLowerCase();
                            legendWindow.body.setHTML(img.outerHTML);
                            legendWindow.show();

                            layerEdit = Ext.getCmp('layerEdit');
                            console.log(layerEdit);
                            layerEdit.layer = layer;
                            layerEdit.opacitySlider.setValue(wmsLayer.opacity*100);

                            layerEdit.filterSlider.setMaxValue(Math.ceil(layer.maxValue));
                            layerEdit.filterSlider.setMinValue(Math.floor(layer.minValue));

                            layerEdit.setTitle('Filter for ' +layer.title);
                            
                            if (!wmsLayer.sliderHighValue) {
                                layerEdit.filterSlider.setValue(0, layer.minValue, true);
                                layerEdit.filterSlider.setValue(1, layer.maxValue, true);
                            }
                            else{
                                layerEdit.filterSlider.setValue(0, wmsLayer.sliderLowValue, true);
                                layerEdit.filterSlider.setValue(1, wmsLayer.sliderHighValue, true);
                            }

                            var resultPanel = Ext.getCmp('resultPanel');
                            resultPanel.reloadLayers(layer.name.toLowerCase());


                            var columns = resultPanel.columns;



                        }
                        
                    }

                }
            }
        });
        
        this.selModel = sm;

        this.columns = [
            {header: 'Layer', dataIndex: 'title', flex: 3},
            {header: 'Categoría', dataIndex: 'layer_category_name', flex: 1}
        ];

        this.callParent(arguments);
    }
});