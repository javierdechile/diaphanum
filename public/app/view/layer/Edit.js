Ext.define('App.view.layer.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.layerEdit',
    title: 'Filter',
    layout: 'anchor',
    id: 'layerEdit',
    width: 300,
    height: 130,
    bodyPadding: 20,

    exportToPdf: function(){


            var resultPanelData = Ext.getCmp('resultPanel').store.data.items

            for (var i = resultPanelData.length - 1; i >= 0; i--) {
                console.log(resultPanelData[i].data)
            };

    },

    initComponent: function() {

        this.opacitySlider = Ext.create('Ext.slider.Single', {
                        minValue: 0,
                        width: 250,
                        maxValue: 100,
                        constrainThumbs: true,
                        fieldLabel: 'Opacidad (%)',
                        name: 'opacity'
                    });

        this.opacitySlider.on('change', function(){
                wmsLayer = App.model.Map.getLayer(this.layer.name);
                sliderValue = this.opacitySlider.thumbs[0].value;
                wmsLayer.setOpacity(sliderValue/100)
        }, this);

        this.filterSlider = Ext.create('Ext.slider.Multi', {
            width: 250,
            values: [0,100],
            increment: 0.5,
            minValue: 0,
            maxValue: 100,
            constrainThumbs: true,
            fieldLabel: 'Rango de filtro'
        });


        this.filterSlider.on('changecomplete', function(){

            var resultPanel = Ext.getCmp('resultPanel');
            var filter = this.layer.variable.toLowerCase()
            resultPanel.reloadLayers(filter);

        }, this);

        this.items = [
                    this.opacitySlider,
                    this.filterSlider
        ];

        this.callParent(arguments);
    }
});