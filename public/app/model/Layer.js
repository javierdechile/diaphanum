Ext.define('App.model.Layer', {
	extend: 'Ext.data.Model',
	fields: [

	//rails g scaffold Layer name:string type:string opacity:integer style:string title:string maxValue:integer minValue:integer description:string formField:references show:boolean

	{
		name: 'name'
	},
	{
		name: 'variable'
	},
	{
		name: 'layer_category_name'
	},
	{
		name: 'opacity'
	},
	{
		name: 'style'
	},
	{
		name: 'title'
	},
	{
		name: 'maxValue'
	},
	{
		name: 'minValue'
	},
	{
		name: 'description'
	},
	{
		name: 'show'
	}
	],
	constructor: function(){
		
		this.callParent(arguments);
	}
});