Ext.define('App.model.Result', {
	extend: 'Ext.data.Model',

    fields: [

        {
            name: 'comuna', type: 'integer'
        },{
            name: 'name', type: 'string'
        },{
            name: 'edad_mean', type: 'float'
        },{
            name: 'edad_median', type: 'float'
        },{
            name: 'edad_standard_deviation', type: 'float'
        },{
            name: 'ecivil_mode', type: 'float'
        },{
            name: 'hombre_percentage', type: 'float'
        },{
            name: 'mujer_percentage', type: 'float'
        },{
            name: 'soltero_percentage', type: 'float'
        },{
            name: 'soltera_percentage', type: 'float'
        },{
            name: 'casado_percentage', type: 'float'
        },{
            name: 'casada_percentage', type: 'float'
        },{
            name: 'emparejado_percentage', type: 'float'
        },{
            name: 'emparejada_percentage', type: 'float'
        },{
            name: 'nucleo_mean', type: 'float'
        },{
            name: 'nucleo_median', type: 'float'
        },{
            name: 'nucleo_standard_deviation', type: 'float'
        },{
            name: 'pco2_mode', type: 'float'
        },{
            name: 'numper_mean', type: 'float'
        },{
            name: 'numper_median', type: 'float'
        },{
            name: 'numper_standard_deviation', type: 'float'
        },{
            name: 'e6a_mode', type: 'float'
        },{
            name: 'e7_mean', type: 'float'
        },{
            name: 'e7_median', type: 'float'
        },{
            name: 'e7_standard_deviation', type: 'float'
        },{
            name: 'e7_mode', type: 'float'
        },{
            name: 'educ_mean', type: 'float'
        },{
            name: 'educ_median', type: 'float'
        },{
            name: 'educ_standard_deviation', type: 'float'
        },{
            name: 'educ_mode', type: 'float'
        },{
            name: 'asiste_mean', type: 'float'
        },{
            name: 'asiste_median', type: 'float'
        },{
            name: 'asiste_standard_deviation', type: 'float'
        },{
            name: 'tot_hogviv_mean', type: 'float'
        },{
            name: 'y29a_percentage', type: 'float'
        },{
            name: 'y29b1_percentage', type: 'float'
        },{
            name: 'y29b2_percentage', type: 'float'
        },{
            name: 'y29b3_percentage', type: 'float'
        },{
            name: 'y29b4_percentage', type: 'float'
        },{
            name: 'y20a_percentage', type: 'float'
        },{
            name: 'y30b_percentage', type: 'float'
        },{
            name: 'y30c_percentage', type: 'float'
        },{
            name: 'y30d_percentage', type: 'float'
        },{
            name: 'y30e_percentage', type: 'float'
        },{
            name: 'corte_percentage', type: 'float'
        },{
            name: 'qaut_mean', type: 'float'
        },{
            name: 'daut_mean', type: 'float'
        },{
            name: 'yataj_mean', type: 'float'
        },{
            name: 'yathaj_mean', type: 'float'
        },{
            name: 'ytotaj_mean', type: 'float'
        },{
            name: 'ytothaj_mean', type: 'float'
        },{
            name: 'ymoneaj_mean', type: 'float'
        },{
            name: 'yaimhaj_mean', type: 'float'
        },{
            name: 'r10_percentage', type: 'float'
        },{
            name: 'r11a_percentage', type: 'float'
        },{
            name: 'r11b_percentage', type: 'float'
        },{
            name: 'r12_percentage', type: 'float'
        },{
            name: 'r13a_percentage', type: 'float'
        },{
            name: 'r13b_percentage', type: 'float'
        },{
            name: 'r13c_percentage', type: 'float'
        },{
            name: 'r13d_percentage', type: 'float'
        },{
            name: 'r13e_percentage', type: 'float'
        },{
            name: 'r13f_percentage', type: 'float'
        },{
            name: 'r14a_percentage', type: 'float'
        },{
            name: 'r14b_percentage', type: 'float'
        },{
            name: 'r14c_percentage', type: 'float'
        },{
            name: 'r14d_percentage', type: 'float'
        },{
            name: 'r14e_percentage', type: 'float'
        },{
            name: 'r16_mode', type: 'float'
        },{
            name: 'r17_mean', type: 'float'
        },{
            name: 'r18b_percentage', type: 'float'
        },{
            name: 'r18c_percentage', type: 'float'
        },{
            name: 'r18d_percentage', type: 'float'
        },{
            name: 'r18e_percentage', type: 'float'
        },{
            name: 'r18f_percentage', type: 'float'
        },{
            name: 'r18g_percentage', type: 'float'
        },{
            name: 'r18h_percentage', type: 'float'
        },{
            name: 'r19_percentage', type: 'float'
        },{
            name: 'r20_mean', type: 'float'
        },{
            name: 'v1_mean', type: 'float'
        },{
            name: 'v2_mean', type: 'float'
        },{
            name: 'v2frent_mean', type: 'float'
        },{
            name: 'v2fo_mean', type: 'float'
        },{
            name: 'v3_percentage', type: 'float'
        },{
            name: 'v4_percentage', type: 'float'
        },{
            name: 'v5_mean', type: 'float'
        },{
            name: 'v5m_mean', type: 'float'
        }
    ],

	constructor: function(){
		
		this.callParent(arguments);
	}
});