Ext.define('App.controller.Layers', {
    extend: 'Ext.app.Controller',
	views: ['layer.List', 'layer.Edit'],
	stores: ['Layers'],
    models: ['Layer'],
    
    init: function() {
        this.control({
            'layerList': {
                itemdblclick: this.editLayer
            }
        });
    },

    editLayer: function(grid, record) {
        var view = Ext.widget('layerEdit',{
            layer : record.data,
            title: 'Edit '+record.data.title
        });
        view.down('form').loadRecord(record);
    },

    onPanelRendered: function() {
        console.log('The panel was rendered');
    }
});