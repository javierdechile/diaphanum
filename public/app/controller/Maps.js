Ext.define('App.controller.Maps', {
    extend: 'Ext.app.Controller',

    views: ['map.Panel'],
    models: ['Map'],

    init: function() {

    	this.control({
			'mapPanel': {
				afterrender: this.afterrenderPanel
			}

    })},

   	afterrenderPanel: function(panel){

		var gmap = new OpenLayers.Layer.Google(
			"Google Streets", // the default
			{numZoomLevels: 20, "sphericalMercator": true }

		);

  		var mapnik  = new OpenLayers.Layer.OSM();                // OpenStreetMap Layer

  		var wgs84 = new OpenLayers.Projection("EPSG:4326");
  		var mercator = new OpenLayers.Projection("EPSG:900913");

		var options = {
		    projection: mercator,
		    displayProjection: wgs84,
		    units: 'degrees'
		};

		var map = new OpenLayers.Map(panel.body.dom, options);
		App.model.Map.setMap(map);

		
		map.setStyle = function setStyle(style){
		    var img = document.createElement("IMG");
		    img.src = '/geoserver/opengeo/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=30&HEIGHT=20&STRICT=false&style='+style;
		    document.getElementById('image').innerHTML = img.outerHTML
		    // we may be switching style on setup
		    if(taxes == null)
		      return;

		    taxes.mergeNewParams({
		      styles: style
		    });
	  	}



		//modifyFeatureControl.mode |=OpenLayers.Control.ModifyFeature.DRAG;

		
		// var clickFeatureInfo = new OpenLayers.Control.WMSGetFeatureInfo({
		// 	url: '/geoserver/opengeo/wms', 
		// 	autoActivate: true,
		// 	title: 'Identify features by clicking',
		// 	layers: [taxes],
		// 	formatOptions: {
		// 		typeName: 'municipios', 
		// 		featureNS: 'opengeo'
		// 	},
		// 	queryVisible: true,
		// 	eventListeners: {
		// 		getfeatureinfo: function(event) {
		// 			Ext.create('Ext.window.Window', {
		// 			title: 'Hello',
		// 			height: 200,
		// 			width: 400,
		// 			layout: 'fit',
		// 			html: event.text
		// 			}).show();

		// 		}
		// 	}
		// });



		// map.addControl(clickFeatureInfo);
		// clickFeatureInfo.activate();

		// build up all controls
		// var kinetic = new OpenLayers.Control.Navigation(
  //           {dragPanOptions: {enableKinetic: true}}
  //       );

  //       map.addControl(kinetic);
  //       kinetic.activate();
		//map.addControl(toolbar);
		//map.addControl(new OpenLayers.Control.LayerSwitcher());


		//map.addLayers([taxes, gmap, layer]);

		App.model.Map.addLayers({
			gmap 	: gmap,
		})

		var lonlat = new OpenLayers.LonLat(-7871200, -3957700);
		var initZoomLevel = 10
		map.setCenter(lonlat, initZoomLevel);

		panel.on("resize", function(){
		map.updateSize();

	});

	}


});