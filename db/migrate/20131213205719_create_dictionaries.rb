class CreateDictionaries < ActiveRecord::Migration
  def change
    create_table :dictionaries do |t|
      t.string :variable
      t.string :label
      t.string :comment

      t.integer :layer_category_id

      t.timestamps
    end
  end
end
