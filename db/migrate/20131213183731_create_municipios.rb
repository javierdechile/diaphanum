class CreateMunicipios < ActiveRecord::Migration
  def change
    create_table :municipios do |t|
      t.integer :comuna
      t.string :name
      t.integer :population
      t.float :edad_mean
      t.float :edad_median
      t.float :edad_standard_deviation
      t.float :ecivil_mode
      t.float :hombre_percentage
      t.float :mujer_percentage
      t.float :soltero_percentage
      t.float :soltera_percentage
      t.float :casado_percentage
      t.float :casada_percentage
      t.float :emparejado_percentage
      t.float :emparejada_percentage
      t.float :nucleo_mean
      t.float :nucleo_median
      t.float :nucleo_standard_deviation
      t.float :pco2_mode
      t.float :numper_mean
      t.float :numper_median
      t.float :numper_standard_deviation
      t.float :e6a_mode
      t.float :e7_mean
      t.float :e7_median
      t.float :e7_standard_deviation
      t.float :e7_mode
      t.float :educ_mean
      t.float :educ_median
      t.float :educ_standard_deviation
      t.float :educ_mode
      t.float :asiste_mean
      t.float :asiste_median
      t.float :asiste_standard_deviation

      t.float :tot_hogviv_mean # personas que viven en la vivienda
      t.float :y29a_percentage # Mantiene ahorro 1=si, 2= no, 9=NS/NR
      t.float :y29b1_percentage # Ahorro en cuenta para la vivinda
      t.float :y29b2_percentage # Ahorro en cta bancaria
      t.float :y29b3_percentage # APV
      t.float :y29b4_percentage # Ahorro en efectivo
      t.float :y20a_percentage # Prestamos bancarios
      t.float :y30b_percentage # Creditos en cajas de compensación
      t.float :y30c_percentage # Avance en efectivo casas comerciales
      t.float :y30d_percentage # Préstamo amigos y parientes
      t.float :y30e_percentage # Crédito con prestamistas o fiado
      t.float :corte_percentage # Pobres extremos = 1, Pobres no extremos = 2, 3 = no pobres
      t.float :qaut_mean # Quintil de ingreso autónomo (1 al 5)
      t.float :daut_mean # Decil de ingreso autónomo (1 al 10)
      t.float :yataj_mean # Ingreso autónomo
      t.float :yathaj_mean # Ingreso autónomo, hogar
      t.float :ytotaj_mean # Ingreso total
      t.float :ytothaj_mean # Ingreso total hogar
      t.float :ymoneaj_mean # Ingreso monetario
      t.float :yaimhaj_mean # Alquiler imputado, hogar
      t.float :r10_percentage # Vehículo particular (1,2 o 9)
      t.float :r11a_percentage # Cantidad de vehículos para uso laboral
      t.float :r11b_percentage # Cantidad de vehículos para uso particular
      t.float :r12_percentage # Seguro para su vehículo (excl SOAP) (1,2,9)
      t.float :r13a_percentage # Tiene Lavadora automática (1,2 o 9)
      t.float :r13b_percentage # Refrigerador
      t.float :r13c_percentage # Calefont
      t.float :r13d_percentage # Teléfono fijo
      t.float :r13e_percentage # TV de pago
      t.float :r13f_percentage # Computador
      t.float :r14a_percentage # Internet en la vivienda (B.Ancha fija contrato)
      t.float :r14b_percentage # Internet en la vivienda (B.Ancha fija prepago)
      t.float :r14c_percentage # Internet en la vivienda (B.Ancha móvil contrato)
      t.float :r14d_percentage # Internet en la vivienda (B.Ancha móvil prepago)
      t.float :r14e_percentage # Internet en la vivienda (smartphone)
      t.float :r16_mode # Donde utiliza internet (1= hogar, 2= trabajo, 3=estab.educacional, 4=infocentro, 5=cibercafe)
      t.float :r17_mean # Frecuencia de uso de internet (1=diariamente, 2=semanalmente, 3= mensual, 4=menos que mensual)
      t.float :r18b_percentage # Uso de internet para comunicacion escrita
      t.float :r18c_percentage # Uso de internet para comunicación por voz
      t.float :r18d_percentage # Uso de internet para entretenimiento
      t.float :r18e_percentage # Uso de internet para comercio electrónico
      t.float :r18f_percentage # Uso de internet para banca electrónica
      t.float :r18g_percentage # Uso de internet para capacitación
      t.float :r18h_percentage # Uso de internet para tramites públicos
      t.float :r19_percentage # Teléfono móvil en uso (1=prepago, 2=contrato, 3=no, 9=NS/NR)
      t.float :r20_mean # Cuán satisfecho está con su vida (1 al 10)
      t.float :v1_mean # Cantidad de viviendas en el sitio (0 al 23)
      t.float :v2_mean # Metros cuadrados que tiene el sitio (1=menos de 100, 2= de 100 a 200, 3=200 a 300,4,5 y 9=NS)
      t.float :v2frent_mean # Metros de frente
      t.float :v2fo_mean # Metros de fondo
      t.float :v3_percentage # Ocupacion del sitio (1=propio,2=pagandose,3=compartido pagado, 4=compartido pagandose, 5=arrendado con contrato, 6=arrendado sin contrato, 7=cedido por trabajo, 8=cedido por familiar, 9=usufructo, 10=ocupación irregular, 11=poseedor irregular)
      t.float :v4_percentage # Alguien en el hogar es el responsable del sitio (1=si dueño, 2=si arriendo, 3=si cesión, 4=no)
      t.float :v5_mean # Metros cuadrados de la vivienda
      t.float :v5m_mean # Metros cuadrados de la vivienda      



      t.timestamps
    end
  end
end
