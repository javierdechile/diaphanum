class CreateLayerCategories < ActiveRecord::Migration
  def change
    create_table :layer_categories do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
