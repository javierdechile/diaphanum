class CreateLayers < ActiveRecord::Migration
  def change
    create_table :layers do |t|
      t.string :description
      t.float :maxValue
      t.float :minValue
      t.string :name
      t.float :opacity
      t.boolean :show
      t.string :style
      t.string :title
      t.string :variable

      t.integer :layer_category_id

      t.timestamps
    end
  end
end
