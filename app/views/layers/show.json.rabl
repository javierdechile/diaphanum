object @layer
attributes *Layer.column_names

node(:layer_category_name) { |layer| layer.layer_category.name}