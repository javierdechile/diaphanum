object @layer_category
attributes :id, :name

child :layers do
	attributes :description, :maxValue, :minValue, :name, :opacity, :show, :style, :title, :variable, :layer_category_id
end
