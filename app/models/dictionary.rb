class Dictionary < ActiveRecord::Base
  attr_accessible :comment, :label, :variable, :layer_category_id
  belongs_to :layer_category
end
