class LayerCategory < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :dictionaries
  has_many :layers
end
