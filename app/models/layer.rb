class Layer < ActiveRecord::Base
  attr_accessible :description, :maxValue, :minValue, :name, :opacity, :show, :style, :title, :variable, :layer_category_id
  belongs_to :layer_category
end
