#!/bin/env ruby
# encoding: utf-8

LayerCategory.delete_all

LayerCategory.create :name => "Demográficas"
LayerCategory.create :name => "Educación"
LayerCategory.create :name => "Salud"
LayerCategory.create :name => "Vivienda"
LayerCategory.create :name => "Economía"
LayerCategory.create :name => "Tecnología"

demografica = LayerCategory.find_by_name('Demográficas');
educacion = LayerCategory.find_by_name('Educación');
salud = LayerCategory.find_by_name('Salud');
vivienda = LayerCategory.find_by_name('Vivienda');
economia = LayerCategory.find_by_name('Economía');
tecnologia = LayerCategory.find_by_name('Tecnología');


Dictionary.delete_all

Dictionary.create :variable => :edad_mean, :label => 'Edad Promedio', :layer_category_id => demografica.id
Dictionary.create :variable => :edad_median, :label => 'Edad (media)', :layer_category_id => demografica.id
Dictionary.create :variable => :edad_standard_deviation, :label => 'Edad (desv. standard)', :layer_category_id => demografica.id

Dictionary.create :variable => :hombres_percentage, :label => 'Hombres (%)', :layer_category_id => demografica.id
Dictionary.create :variable => :mujeres_percentage, :label => 'Mujeres (%)', :layer_category_id => demografica.id

Dictionary.create :variable => :ecivil_mode, :label => 'Estado civil (moda)', :layer_category_id => demografica.id
Dictionary.create :variable => :soltero_percentage, :label => 'Solteros (%)', :layer_category_id => demografica.id
Dictionary.create :variable => :soltera_percentage, :label => 'Solteras (%)', :layer_category_id => demografica.id
Dictionary.create :variable => :casado_percentage, :label => 'Casados (%)', :layer_category_id => demografica.id
Dictionary.create :variable => :casada_percentage, :label => 'Casadas (%)', :layer_category_id => demografica.id
Dictionary.create :variable => :emparejado_percentage, :label => 'Hombres en pareja o convivientes (%)', :layer_category_id => demografica.id
Dictionary.create :variable => :emparejada_percentage, :label => 'Mujeres en pareja o convivientes (%)', :layer_category_id => demografica.id

Dictionary.create :variable => :nucleo_mean, :label => 'Promedio de núcleos familiares', :layer_category_id => demografica.id
Dictionary.create :variable => :nucleo_median, :label => 'Mediana de núcleos familiares', :layer_category_id => demografica.id
Dictionary.create :variable => :nucleo_standard_deviation, :label => 'Desviación standard de núcleos familiares', :layer_category_id => demografica.id

Dictionary.create :variable => :pco2_mode, :label => 'Jefe de núcleo familiar mode', :layer_category_id => demografica.id

Dictionary.create :variable => :numper_mean, :label => 'Promedio de personas en la vivienda', :layer_category_id => demografica.id
Dictionary.create :variable => :numper_median, :label => 'Mediana de personas en la vivienda', :layer_category_id => demografica.id
Dictionary.create :variable => :numper_standard_deviation, :label => 'Desv. Standard de personas en la vivienda', :layer_category_id => demografica.id

Dictionary.create :variable => :e6a_mode, :label => 'Nivel educacional actual (moda)', :layer_category_id => educacion.id

Dictionary.create :variable => :e7_mean, :label => 'Duración carrera (promedio)', :layer_category_id => educacion.id
Dictionary.create :variable => :e7_median, :label => 'Duración carrera (mediana)', :layer_category_id => educacion.id
Dictionary.create :variable => :e7_standard_deviation, :label => 'Duración carrera (desv. standard)', :layer_category_id => educacion.id
Dictionary.create :variable => :e7_mode, :label => 'Duración carrera (moda)', :layer_category_id => educacion.id

Dictionary.create :variable => :esc_mean, :label => 'Escolaridad promedio', :layer_category_id => educacion.id
Dictionary.create :variable => :esc_median, :label => 'Escolaridad median', :layer_category_id => educacion.id
Dictionary.create :variable => :esc_standard_deviation, :label => 'Escolaridad standard_deviation', :layer_category_id => educacion.id

Dictionary.create :variable => :educ_mean, :label => 'Nivel educacional mean', :layer_category_id => educacion.id
Dictionary.create :variable => :educ_median, :label => 'Nivel educacional median', :layer_category_id => educacion.id
Dictionary.create :variable => :educ_standard_deviation, :label => 'Nivel educacional standard_deviation', :layer_category_id => educacion.id
Dictionary.create :variable => :educ_mode, :label => 'Nivel educacional mode', :layer_category_id => educacion.id

Dictionary.create :variable => :asiste_mean, :label => 'asiste mean', :layer_category_id => educacion.id
Dictionary.create :variable => :asiste_median, :label => 'asiste median', :layer_category_id => educacion.id
Dictionary.create :variable => :asiste_standard_deviation, :label => 'asiste standard_deviation', :layer_category_id => educacion.id


Dictionary.create :variable => :tot_hogviv_mean, :label => "personas que viven en la vivienda", :layer_category_id => vivienda.id
Dictionary.create :variable => :y29a_percentage, :label => "Mantiene ahorro 1=si, 2= no, 9=NS/NR",:layer_category_id => vivienda.id
Dictionary.create :variable => :y29b1_percentage, :label => "Ahorro en cuenta para la vivienda", :layer_category_id => vivienda.id
Dictionary.create :variable => :y29b2_percentage, :label => "Ahorro en cta bancaria", :layer_category_id => economia.id
Dictionary.create :variable => :y29b3_percentage, :label => "APV", :layer_category_id => economia.id
Dictionary.create :variable => :y29b4_percentage, :label => "Ahorro en efectivo", :layer_category_id => economia.id
Dictionary.create :variable => :y20a_percentage, :label => "Prestamos bancarios", :layer_category_id => economia.id
Dictionary.create :variable => :y30b_percentage, :label => "Creditos en cajas de compensación", :layer_category_id => economia.id
Dictionary.create :variable => :y30c_percentage, :label => "Avance en efectivo casas comerciales", :layer_category_id => economia.id
Dictionary.create :variable => :y30d_percentage, :label => "Préstamo amigos y parientes", :layer_category_id => economia.id
Dictionary.create :variable => :y30e_percentage, :label => "Crédito con prestamistas o fiado", :layer_category_id => economia.id
Dictionary.create :variable => :corte_percentage, :label => "Pobres extremos = 1, Pobres no extremos = 2, 3 = no pobres",:layer_category_id => economia.id
Dictionary.create :variable => :qaut_mean, :label => "Quintil de ingreso autónomo (1 al 5)", :layer_category_id => economia.id
Dictionary.create :variable => :daut_mean, :label => "Decil de ingreso autónomo (1 al 10)", :layer_category_id => economia.id
Dictionary.create :variable => :yataj_mean, :label => "Ingreso autónomo", :layer_category_id => economia.id
Dictionary.create :variable => :yathaj_mean, :label => "Ingreso autónomo, hogar", :layer_category_id => economia.id
Dictionary.create :variable => :ytotaj_mean, :label => "Ingreso total", :layer_category_id => economia.id
Dictionary.create :variable => :ytothaj_mean, :label => "Ingreso total hogar", :layer_category_id => economia.id
Dictionary.create :variable => :ymoneaj_mean, :label => "Ingreso monetario", :layer_category_id => economia.id
Dictionary.create :variable => :yaimhaj_mean, :label => "Alquiler imputado, hogar", :layer_category_id => economia.id
Dictionary.create :variable => :r10_percentage, :label => "Vehículo particular (1,2 o 9)", :layer_category_id => economia.id
Dictionary.create :variable => :r11a_percentage, :label => "Cantidad de vehículos para uso laboral", :layer_category_id => economia.id
Dictionary.create :variable => :r11b_percentage, :label => "Cantidad de vehículos para uso particular", :layer_category_id => economia.id
Dictionary.create :variable => :r12_percentage, :label => "Seguro para su vehículo (excl SOAP) (1,2,9)", :layer_category_id => economia.id
Dictionary.create :variable => :r13a_percentage, :label => "Tiene Lavadora automática (1,2 o 9)", :layer_category_id => vivienda.id
Dictionary.create :variable => :r13b_percentage, :label => "Refrigerador", :layer_category_id => vivienda.id
Dictionary.create :variable => :r13c_percentage, :label => "Calefont", :layer_category_id => vivienda.id
Dictionary.create :variable => :r13d_percentage, :label => "Teléfono fijo", :layer_category_id => vivienda.id
Dictionary.create :variable => :r13e_percentage, :label => "TV de pago", :layer_category_id => vivienda.id
Dictionary.create :variable => :r13f_percentage, :label => "Computador", :layer_category_id => vivienda.id
Dictionary.create :variable => :r14a_percentage, :label => "Internet en la vivienda (B.Ancha fija contrato)", :layer_category_id => vivienda.id
Dictionary.create :variable => :r14b_percentage, :label => "Internet en la vivienda (B.Ancha fija prepago)", :layer_category_id => vivienda.id
Dictionary.create :variable => :r14c_percentage, :label => "Internet en la vivienda (B.Ancha móvil contrato)", :layer_category_id => vivienda.id
Dictionary.create :variable => :r14d_percentage, :label => "Internet en la vivienda (B.Ancha móvil prepago)", :layer_category_id => vivienda.id
Dictionary.create :variable => :r14e_percentage, :label => "Internet en la vivienda (smartphone)", :layer_category_id => vivienda.id
Dictionary.create :variable => :r16_mode, :label => "Donde utiliza internet (1= hogar, 2= trabajo, 3=estab.educacional, 4=infocentro, 5=cibercafe)", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r17_mean, :label => "Frecuencia de uso de internet (1=diariamente, 2=semanalmente, 3= mensual, 4=menos que mensual)", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18b_percentage, :label => "Uso de internet para comunicacion escrita", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18c_percentage, :label => "Uso de internet para comunicación por voz", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18d_percentage, :label => "Uso de internet para entretenimiento", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18e_percentage, :label => "Uso de internet para comercio electrónico", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18f_percentage, :label => "Uso de internet para banca electrónica", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18g_percentage, :label => "Uso de internet para capacitación", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r18h_percentage, :label => "Uso de internet para tramites públicos", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r19_percentage, :label => "Teléfono móvil en uso (1=prepago, 2=contrato, 3=no, 9=NS/NR)", :layer_category_id => tecnologia.id
Dictionary.create :variable => :r20_mean, :label => "Cuán satisfecho está con su vida (1 al 10)", :layer_category_id => demografica.id
Dictionary.create :variable => :v1_mean, :label => "Cantidad de viviendas en el sitio (0 al 23)", :layer_category_id => vivienda.id
Dictionary.create :variable => :v2_mean, :label => "Metros cuadrados que tiene el sitio (1=menos de 100, 2= de 100 a 200, 3=200 a 300,4,5 y 9=NS)", :layer_category_id => vivienda.id
Dictionary.create :variable => :v2frent_mean, :label => "Metros de frente", :layer_category_id => vivienda.id
Dictionary.create :variable => :v2fo_mean, :label => "Metros de fondo", :layer_category_id => vivienda.id
Dictionary.create :variable => :v3_percentage, :label => "Ocupacion del sitio (1=propio,2=pagandose,3=compartido pagado, 4=compartido pagandose, 5=arrendado con contrato, 6=arrendado sin contrato, 7=cedido por trabajo, 8=cedido por familiar, 9=usufructo, 10=ocupación irregular, 11=poseedor irregular)", :layer_category_id => vivienda.id
Dictionary.create :variable => :v4_percentage, :label => "Alguien en el hogar es el responsable del sitio (1=si dueño, 2=si arriendo, 3=si cesión, 4=no)", :layer_category_id => vivienda.id
Dictionary.create :variable => :v5_mean, :label => "Metros cuadrados de la vivienda", :layer_category_id => vivienda.id