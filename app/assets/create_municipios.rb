#!/bin/env ruby
# encoding: utf-8

Municipio.delete_all
Municipio.create(:comuna => 1101, :name => "Iquique")
Municipio.create(:comuna => 1107, :name => "Alto Hospicio")
Municipio.create(:comuna => 1401, :name => "Pozo Almonte")
Municipio.create(:comuna => 1402, :name => "Camiña")
Municipio.create(:comuna => 1403, :name => "Colchane")
Municipio.create(:comuna => 1404, :name => "Huara")
Municipio.create(:comuna => 1405, :name => "Pica")
Municipio.create(:comuna => 2101, :name => "Antofagasta")
Municipio.create(:comuna => 2102, :name => "Mejillones")
Municipio.create(:comuna => 2103, :name => "Sierra Gorda")
Municipio.create(:comuna => 2104, :name => "Taltal")
Municipio.create(:comuna => 2201, :name => "Calama")
Municipio.create(:comuna => 2202, :name => "Ollagüe")
Municipio.create(:comuna => 5101, :name => "Valparaíso")
Municipio.create(:comuna => 5102, :name => "Casablanca")
Municipio.create(:comuna => 5103, :name => "Concón")
Municipio.create(:comuna => 5104, :name => "Juan Fernández")
Municipio.create(:comuna => 5105, :name => "Puchuncaví")
Municipio.create(:comuna => 5107, :name => "Quintero")
Municipio.create(:comuna => 5109, :name => "Viña del Mar")
Municipio.create(:comuna => 5201, :name => "Isla de Pascua")
Municipio.create(:comuna => 5301, :name => "Los Andes")
Municipio.create(:comuna => 5302, :name => "Calle Larga")
Municipio.create(:comuna => 5303, :name => "Rinconada")
Municipio.create(:comuna => 5304, :name => "San Esteban")
Municipio.create(:comuna => 5401, :name => "La Ligua")
Municipio.create(:comuna => 5402, :name => "Cabildo")
Municipio.create(:comuna => 5403, :name => "Papudo")
Municipio.create(:comuna => 5404, :name => "Petorca")
Municipio.create(:comuna => 5405, :name => "Zapallar")
Municipio.create(:comuna => 5501, :name => "Quillota")
Municipio.create(:comuna => 5502, :name => "La Calera")
Municipio.create(:comuna => 5503, :name => "Hijuelas")
Municipio.create(:comuna => 5504, :name => "La Cruz")
Municipio.create(:comuna => 5506, :name => "Nogales")
Municipio.create(:comuna => 5601, :name => "San Antonio")
Municipio.create(:comuna => 5602, :name => "Algarrobo")
Municipio.create(:comuna => 5603, :name => "Cartagena")
Municipio.create(:comuna => 5604, :name => "El Quisco")
Municipio.create(:comuna => 5605, :name => "El Tabo")
Municipio.create(:comuna => 5606, :name => "Santo Domingo")
Municipio.create(:comuna => 5701, :name => "San Felipe")
Municipio.create(:comuna => 5702, :name => "Catemu")
Municipio.create(:comuna => 5703, :name => "Llaillay")
Municipio.create(:comuna => 5704, :name => "Panquehue")
Municipio.create(:comuna => 5705, :name => "Putaendo")
Municipio.create(:comuna => 5706, :name => "Santa María")
Municipio.create(:comuna => 5801, :name => "Quilpué")
Municipio.create(:comuna => 5802, :name => "Limache")
Municipio.create(:comuna => 5803, :name => "Olmué")
Municipio.create(:comuna => 5804, :name => "Villa Alemana")
Municipio.create(:comuna => 6101, :name => "Rancagua")
Municipio.create(:comuna => 6102, :name => "Codegua")
Municipio.create(:comuna => 6103, :name => "Coinco")
Municipio.create(:comuna => 6104, :name => "Coltauco")
Municipio.create(:comuna => 6105, :name => "Doñihue")
Municipio.create(:comuna => 6106, :name => "Graneros")
Municipio.create(:comuna => 6107, :name => "Las Cabras")
Municipio.create(:comuna => 6108, :name => "Machalí")
Municipio.create(:comuna => 6109, :name => "Malloa")
Municipio.create(:comuna => 6110, :name => "Mostazal")
Municipio.create(:comuna => 6111, :name => "Olivar")
Municipio.create(:comuna => 6112, :name => "Peumo")
Municipio.create(:comuna => 6113, :name => "Pichidegua")
Municipio.create(:comuna => 6114, :name => "Quinta de Tilcoco")
Municipio.create(:comuna => 6115, :name => "Rengo")
Municipio.create(:comuna => 6116, :name => "Requínoa")
Municipio.create(:comuna => 6117, :name => "San Vicente")
Municipio.create(:comuna => 6201, :name => "Pichilemu")
Municipio.create(:comuna => 6202, :name => "La Estrella")
Municipio.create(:comuna => 6203, :name => "Litueche")
Municipio.create(:comuna => 6204, :name => "Marchihue")
Municipio.create(:comuna => 6205, :name => "Navidad")
Municipio.create(:comuna => 6206, :name => "Paredones")
Municipio.create(:comuna => 6301, :name => "San Fernando")
Municipio.create(:comuna => 6302, :name => "Chépica")
Municipio.create(:comuna => 6303, :name => "Chimbarongo")
Municipio.create(:comuna => 6304, :name => "Lolol")
Municipio.create(:comuna => 6305, :name => "Nancagua")
Municipio.create(:comuna => 6306, :name => "Palmilla")
Municipio.create(:comuna => 6307, :name => "Peralillo")
Municipio.create(:comuna => 6308, :name => "Placilla")
Municipio.create(:comuna => 6309, :name => "Pumanque")
Municipio.create(:comuna => 6310, :name => "Santa Cruz")
Municipio.create(:comuna => 7101, :name => "Talca")
Municipio.create(:comuna => 7102, :name => "Constitución")
Municipio.create(:comuna => 7103, :name => "Curepto")
Municipio.create(:comuna => 7104, :name => "Empedrado")
Municipio.create(:comuna => 7105, :name => "Maule")
Municipio.create(:comuna => 7106, :name => "Pelarco")
Municipio.create(:comuna => 7107, :name => "Pencahue")
Municipio.create(:comuna => 7108, :name => "Río Claro")
Municipio.create(:comuna => 7109, :name => "San Clemente")
Municipio.create(:comuna => 2203, :name => "San Pedro de Atacama")
Municipio.create(:comuna => 2301, :name => "Tocopilla")
Municipio.create(:comuna => 2302, :name => "María Elena")
Municipio.create(:comuna => 3101, :name => "Copiapó")
Municipio.create(:comuna => 3102, :name => "Caldera")
Municipio.create(:comuna => 3103, :name => "Tierra Amarilla")
Municipio.create(:comuna => 3201, :name => "Chañaral")
Municipio.create(:comuna => 3202, :name => "Diego de Almagro")
Municipio.create(:comuna => 3301, :name => "Vallenar")
Municipio.create(:comuna => 3302, :name => "Alto del Carmen")
Municipio.create(:comuna => 3303, :name => "Freirina")
Municipio.create(:comuna => 3304, :name => "Huasco")
Municipio.create(:comuna => 4101, :name => "La Serena")
Municipio.create(:comuna => 4102, :name => "Coquimbo")
Municipio.create(:comuna => 4103, :name => "Andacollo")
Municipio.create(:comuna => 4104, :name => "La Higuera")
Municipio.create(:comuna => 4105, :name => "Paiguano")
Municipio.create(:comuna => 4106, :name => "Vicuña")
Municipio.create(:comuna => 4201, :name => "Illapel")
Municipio.create(:comuna => 4202, :name => "Canela")
Municipio.create(:comuna => 4203, :name => "Los Vilos")
Municipio.create(:comuna => 4204, :name => "Salamanca")
Municipio.create(:comuna => 4301, :name => "Ovalle")
Municipio.create(:comuna => 4302, :name => "Combarbalá")
Municipio.create(:comuna => 4303, :name => "Monte Patria")
Municipio.create(:comuna => 4304, :name => "Punitaqui")
Municipio.create(:comuna => 4305, :name => "Río Hurtado")
Municipio.create(:comuna => 7110, :name => "San Rafael")
Municipio.create(:comuna => 7201, :name => "Cauquenes")
Municipio.create(:comuna => 7202, :name => "Chanco")
Municipio.create(:comuna => 7203, :name => "Pelluhue")
Municipio.create(:comuna => 7301, :name => "Curicó")
Municipio.create(:comuna => 7302, :name => "Hualañé")
Municipio.create(:comuna => 7303, :name => "Licantén")
Municipio.create(:comuna => 7304, :name => "Molina")
Municipio.create(:comuna => 7305, :name => "Rauco")
Municipio.create(:comuna => 7306, :name => "Romeral")
Municipio.create(:comuna => 7307, :name => "Sagrada")
Municipio.create(:comuna => 7308, :name => "Teno")
Municipio.create(:comuna => 7309, :name => "Vichuquén")
Municipio.create(:comuna => 7401, :name => "Linares")
Municipio.create(:comuna => 7402, :name => "Colbún")
Municipio.create(:comuna => 7403, :name => "Longaví")
Municipio.create(:comuna => 7404, :name => "Parral")
Municipio.create(:comuna => 7405, :name => "Retiro")
Municipio.create(:comuna => 7406, :name => "San Javier")
Municipio.create(:comuna => 7407, :name => "Villa Alegre")
Municipio.create(:comuna => 7408, :name => "Yerbas Buenas")
Municipio.create(:comuna => 8101, :name => "Concepción")
Municipio.create(:comuna => 8102, :name => "Coronel")
Municipio.create(:comuna => 8103, :name => "Chiguayante")
Municipio.create(:comuna => 8104, :name => "Florida")
Municipio.create(:comuna => 8105, :name => "Hualqui")
Municipio.create(:comuna => 8106, :name => "Lota")
Municipio.create(:comuna => 8107, :name => "Penco")
Municipio.create(:comuna => 8108, :name => "San Pedro de la Paz")
Municipio.create(:comuna => 8109, :name => "Santa Juana")
Municipio.create(:comuna => 8110, :name => "Talcahuano")
Municipio.create(:comuna => 8111, :name => "Tomé")
Municipio.create(:comuna => 8112, :name => "Hualpén")
Municipio.create(:comuna => 8201, :name => "Lebu")
Municipio.create(:comuna => 8202, :name => "Arauco")
Municipio.create(:comuna => 8203, :name => "Cañete")
Municipio.create(:comuna => 8204, :name => "Contulmo")
Municipio.create(:comuna => 8205, :name => "Curanilahue")
Municipio.create(:comuna => 8206, :name => "Los álamos")
Municipio.create(:comuna => 8207, :name => "Tirúa")
Municipio.create(:comuna => 8301, :name => "Los ángeles")
Municipio.create(:comuna => 8302, :name => "Antuco")
Municipio.create(:comuna => 8303, :name => "Cabrero")
Municipio.create(:comuna => 8304, :name => "Laja")
Municipio.create(:comuna => 8305, :name => "Mulchén")
Municipio.create(:comuna => 8306, :name => "Nacimiento")
Municipio.create(:comuna => 8307, :name => "Negrete")
Municipio.create(:comuna => 8308, :name => "Quilaco")
Municipio.create(:comuna => 8309, :name => "Quilleco")
Municipio.create(:comuna => 8310, :name => "San Rosendo")
Municipio.create(:comuna => 8311, :name => "Santa Bárbara")
Municipio.create(:comuna => 8312, :name => "Tucapel")
Municipio.create(:comuna => 8313, :name => "Yumbel")
Municipio.create(:comuna => 8314, :name => "Alto Biobío")
Municipio.create(:comuna => 8401, :name => "Chillán")
Municipio.create(:comuna => 8402, :name => "Bulnes")
Municipio.create(:comuna => 8403, :name => "Cobquecura")
Municipio.create(:comuna => 8404, :name => "Coelemu")
Municipio.create(:comuna => 8405, :name => "Coihueco")
Municipio.create(:comuna => 8406, :name => "Chillán Viejo")
Municipio.create(:comuna => 8407, :name => "El Carmen")
Municipio.create(:comuna => 8408, :name => "Ninhue")
Municipio.create(:comuna => 8409, :name => "ñiquén")
Municipio.create(:comuna => 8410, :name => "Pemuco")
Municipio.create(:comuna => 8411, :name => "Pinto")
Municipio.create(:comuna => 8412, :name => "Portezuelo")
Municipio.create(:comuna => 8413, :name => "Quillón")
Municipio.create(:comuna => 8414, :name => "Quirihue")
Municipio.create(:comuna => 8415, :name => "Ránquil")
Municipio.create(:comuna => 8416, :name => "San Carlos")
Municipio.create(:comuna => 8417, :name => "San Fabián")
Municipio.create(:comuna => 8418, :name => "San Ignacio")
Municipio.create(:comuna => 8419, :name => "San Nicolás")
Municipio.create(:comuna => 8420, :name => "Treguaco")
Municipio.create(:comuna => 8421, :name => "Yungay")
Municipio.create(:comuna => 9101, :name => "Temuco")
Municipio.create(:comuna => 9102, :name => "Carahue")
Municipio.create(:comuna => 9103, :name => "Cunco")
Municipio.create(:comuna => 9104, :name => "Curarrehue")
Municipio.create(:comuna => 9105, :name => "Freire")
Municipio.create(:comuna => 9106, :name => "Galvarino")
Municipio.create(:comuna => 9107, :name => "Gorbea")
Municipio.create(:comuna => 9108, :name => "Lautaro")
Municipio.create(:comuna => 9109, :name => "Loncoche")
Municipio.create(:comuna => 9110, :name => "Melipeuco")
Municipio.create(:comuna => 9111, :name => "Nueva Imperial")
Municipio.create(:comuna => 9112, :name => "Padre Las Casas")
Municipio.create(:comuna => 9113, :name => "Perquenco")
Municipio.create(:comuna => 9114, :name => "Pitrufquén")
Municipio.create(:comuna => 9115, :name => "Pucón")
Municipio.create(:comuna => 9116, :name => "Saavedra")
Municipio.create(:comuna => 9117, :name => "Teodoro Schmidt")
Municipio.create(:comuna => 9118, :name => "Toltén")
Municipio.create(:comuna => 9119, :name => "Vilcún")
Municipio.create(:comuna => 9120, :name => "Villarrica")
Municipio.create(:comuna => 9121, :name => "Cholchol")
Municipio.create(:comuna => 9201, :name => "Angol")
Municipio.create(:comuna => 9202, :name => "Collipulli")
Municipio.create(:comuna => 9203, :name => "Curacautín")
Municipio.create(:comuna => 9204, :name => "Ercilla")
Municipio.create(:comuna => 9205, :name => "Lonquimay")
Municipio.create(:comuna => 9206, :name => "Los Sauces")
Municipio.create(:comuna => 9207, :name => "Lumaco")
Municipio.create(:comuna => 9208, :name => "Purén")
Municipio.create(:comuna => 9209, :name => "Renaico")
Municipio.create(:comuna => 9210, :name => "Traiguén")
Municipio.create(:comuna => 9211, :name => "Victoria")
Municipio.create(:comuna => 10101, :name => "Puerto Montt")
Municipio.create(:comuna => 10102, :name => "Calbuco")
Municipio.create(:comuna => 10103, :name => "Cochamó")
Municipio.create(:comuna => 10104, :name => "Fresia")
Municipio.create(:comuna => 10105, :name => "Frutillar")
Municipio.create(:comuna => 10106, :name => "Los Muermos")
Municipio.create(:comuna => 10107, :name => "Llanquihue")
Municipio.create(:comuna => 10108, :name => "Maullín")
Municipio.create(:comuna => 10109, :name => "Puerto Varas")
Municipio.create(:comuna => 10201, :name => "Castro")
Municipio.create(:comuna => 10202, :name => "Ancud")
Municipio.create(:comuna => 10203, :name => "Chonchi")
Municipio.create(:comuna => 10204, :name => "Curaco de Vélez")
Municipio.create(:comuna => 10205, :name => "Dalcahue")
Municipio.create(:comuna => 10206, :name => "Puqueldón")
Municipio.create(:comuna => 10207, :name => "Queilén")
Municipio.create(:comuna => 10208, :name => "Quellón")
Municipio.create(:comuna => 10209, :name => "Quemchi")
Municipio.create(:comuna => 10210, :name => "Quinchao")
Municipio.create(:comuna => 10301, :name => "Osorno")
Municipio.create(:comuna => 10302, :name => "Puerto Octay")
Municipio.create(:comuna => 10303, :name => "Purranque")
Municipio.create(:comuna => 10304, :name => "Puyehue")
Municipio.create(:comuna => 10305, :name => "Río Negro")
Municipio.create(:comuna => 10306, :name => "San Juan de la Costa")
Municipio.create(:comuna => 10307, :name => "San Pablo")
Municipio.create(:comuna => 10401, :name => "Chaitén")
Municipio.create(:comuna => 10402, :name => "Futaleufú")
Municipio.create(:comuna => 10403, :name => "Hualaihué")
Municipio.create(:comuna => 10404, :name => "Palena")
Municipio.create(:comuna => 11101, :name => "Coyhaique")
Municipio.create(:comuna => 11102, :name => "Lago Verde")
Municipio.create(:comuna => 11201, :name => "Aysén")
Municipio.create(:comuna => 11202, :name => "Cisnes")
Municipio.create(:comuna => 11203, :name => "Guaitecas")
Municipio.create(:comuna => 11301, :name => "Cochrane")
Municipio.create(:comuna => 11302, :name => "O’Higgins")
Municipio.create(:comuna => 11303, :name => "Tortel")
Municipio.create(:comuna => 11401, :name => "Chile Chico")
Municipio.create(:comuna => 11402, :name => "Río Ibáñez")
Municipio.create(:comuna => 12101, :name => "Punta Arenas")
Municipio.create(:comuna => 12102, :name => "Laguna Blanca")
Municipio.create(:comuna => 12103, :name => "Río Verde")
Municipio.create(:comuna => 12104, :name => "San Gregorio")
Municipio.create(:comuna => 12202, :name => "Antártica")
Municipio.create(:comuna => 12301, :name => "Porvenir")
Municipio.create(:comuna => 12302, :name => "Primavera")
Municipio.create(:comuna => 12303, :name => "Timaukel")
Municipio.create(:comuna => 12401, :name => "Natales")
Municipio.create(:comuna => 12402, :name => "Torres del Paine")
Municipio.create(:comuna => 13101, :name => "Santiago")
Municipio.create(:comuna => 13102, :name => "Cerrillos")
Municipio.create(:comuna => 13103, :name => "Cerro Navia")
Municipio.create(:comuna => 13104, :name => "Conchalí")
Municipio.create(:comuna => 13105, :name => "El Bosque")
Municipio.create(:comuna => 13106, :name => "Estación Central")
Municipio.create(:comuna => 13107, :name => "Huechuraba")
Municipio.create(:comuna => 13108, :name => "Independencia")
Municipio.create(:comuna => 13109, :name => "La Cisterna")
Municipio.create(:comuna => 13110, :name => "La Florida")
Municipio.create(:comuna => 13111, :name => "La Granja")
Municipio.create(:comuna => 13112, :name => "La Pintana")
Municipio.create(:comuna => 13113, :name => "La Reina")
Municipio.create(:comuna => 13114, :name => "Las Condes")
Municipio.create(:comuna => 13115, :name => "Lo Barnechea")
Municipio.create(:comuna => 13116, :name => "Lo Espejo")
Municipio.create(:comuna => 13117, :name => "Lo Prado")
Municipio.create(:comuna => 13118, :name => "Macul")
Municipio.create(:comuna => 13119, :name => "Maipú")
Municipio.create(:comuna => 13120, :name => "ñuñoa")
Municipio.create(:comuna => 13121, :name => "Pedro Aguirre Cerda")
Municipio.create(:comuna => 13122, :name => "Peñalolén")
Municipio.create(:comuna => 13123, :name => "Providencia")
Municipio.create(:comuna => 13124, :name => "Pudahuel")
Municipio.create(:comuna => 13125, :name => "Quilicura")
Municipio.create(:comuna => 13126, :name => "Quinta Normal")
Municipio.create(:comuna => 13127, :name => "Recoleta")
Municipio.create(:comuna => 13128, :name => "Renca")
Municipio.create(:comuna => 13129, :name => "San Joaquín")
Municipio.create(:comuna => 13130, :name => "San Miguel")
Municipio.create(:comuna => 13131, :name => "San Ramón")
Municipio.create(:comuna => 13132, :name => "Vitacura")
Municipio.create(:comuna => 13201, :name => "Puente Alto")
Municipio.create(:comuna => 13202, :name => "Pirque")
Municipio.create(:comuna => 13203, :name => "San José de Maipo")
Municipio.create(:comuna => 13301, :name => "Colina")
Municipio.create(:comuna => 13302, :name => "Lampa")
Municipio.create(:comuna => 13303, :name => "Tiltil")
Municipio.create(:comuna => 13401, :name => "San Bernardo")
Municipio.create(:comuna => 13402, :name => "Buin")
Municipio.create(:comuna => 13403, :name => "Calera de Tango")
Municipio.create(:comuna => 13404, :name => "Paine")
Municipio.create(:comuna => 13501, :name => "Melipilla")
Municipio.create(:comuna => 13502, :name => "Alhué")
Municipio.create(:comuna => 13503, :name => "Curacaví")
Municipio.create(:comuna => 13504, :name => "María Pinto")
Municipio.create(:comuna => 13505, :name => "San Pedro")
Municipio.create(:comuna => 13601, :name => "Talagante")
Municipio.create(:comuna => 13602, :name => "El Monte")
Municipio.create(:comuna => 13603, :name => "Isla de Maipo")
Municipio.create(:comuna => 13604, :name => "Padre Hurtado")
Municipio.create(:comuna => 13605, :name => "Peñaflor")
Municipio.create(:comuna => 14101, :name => "Valdivia")
Municipio.create(:comuna => 14102, :name => "Corral")
Municipio.create(:comuna => 14103, :name => "Lanco")
Municipio.create(:comuna => 14104, :name => "Los Lagos")
Municipio.create(:comuna => 14105, :name => "Máfil")
Municipio.create(:comuna => 14106, :name => "Mariquina")
Municipio.create(:comuna => 14107, :name => "Paillaco")
Municipio.create(:comuna => 14108, :name => "Panguipulli")
Municipio.create(:comuna => 14201, :name => "La Unión")
Municipio.create(:comuna => 14202, :name => "Futrono")
Municipio.create(:comuna => 14203, :name => "Lago Ranco")
Municipio.create(:comuna => 14204, :name => "Río Bueno")
Municipio.create(:comuna => 15101, :name => "Arica")
Municipio.create(:comuna => 15102, :name => "Camarones")
Municipio.create(:comuna => 15201, :name => "Putre")
Municipio.create(:comuna => 15202, :name => "General Lagos")



municipios = Municipio.all(:order => "comuna desc")#, :limit => 10)

GC.enable
municipios.each do |municipio|

	casen = municipio.casens

	unless casen == nil or casen==[]

		#compact removes nil values
		sexo = DescriptiveStatistics::Stats.new(casen.map{|u| u[:sexo]}.compact)
		edad = DescriptiveStatistics::Stats.new(casen.map{|u| u[:edad]}.compact)
		ecivil = DescriptiveStatistics::Stats.new(casen.map{|u| u[:ecivil]}.compact)
		sexoecivil = DescriptiveStatistics::Stats.new(casen.map{|u| [u[:sexo],u[:ecivil]]}.compact)
		pareja = DescriptiveStatistics::Stats.new(casen.map{|u| u[:pareja]}.compact)
		nucleo = DescriptiveStatistics::Stats.new(casen.map{|u| u[:nucleo]}.compact)
		pco2 = DescriptiveStatistics::Stats.new(casen.map{|u| u[:pco2]}.compact)
		numper = DescriptiveStatistics::Stats.new(casen.map{|u| u[:numper]}.compact)
		e6a = DescriptiveStatistics::Stats.new(casen.map{|u| u[:e6a]}.compact) 
		e7 = DescriptiveStatistics::Stats.new(casen.map{|u| u[:e7]}.compact)
		esc = DescriptiveStatistics::Stats.new(casen.map{|u| u[:esc]}.compact)
		educ = DescriptiveStatistics::Stats.new(casen.map{|u| u[:educ]}.compact)
		asiste = DescriptiveStatistics::Stats.new(casen.map{|u| u[:pco2]}.compact)

	     tot_hogviv = DescriptiveStatistics::Stats.new(casen.map{|u| u[:tot_hogviv]}.compact) # personas que viven en la vivienda
	     y29a = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y29a]}.compact) # Mantiene ahorro 1=si, 2= no, 9=NS/NR
	     y29b1 = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y29b1]}.compact) # Ahorro en cuenta para la vivinda
	     y29b2  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y29b2]}.compact) # Ahorro en cta bancaria
	     y29b3 = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y29b3]}.compact) # APV
	     y29b4 = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y29b4]}.compact) # Ahorro en efectivo
	     y20a  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y20a]}.compact) # Prestamos bancarios
	     y30b  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y30b]}.compact) # Creditos en cajas de compensación
	     y30c  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y30c]}.compact) # Avance en efectivo casas comerciales
	     y30d  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y30d]}.compact) # Préstamo amigos y parientes
	     y30e  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:y30e]}.compact) # Crédito con prestamistas o fiado
	     corte  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:corte]}.compact) # Pobres extremos = 1, Pobres no ext=2
	     qaut  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:qaut]}.compact) # Quintil de ingreso autónomo (1 al 5)
	     daut  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:daut]}.compact) # Decil de ingreso autónomo (1 al 10)
	     yataj  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:yataj]}.compact) # Ingreso autónomo
	     yathaj  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:yathaj]}.compact) # Ingreso autónomo, hogar
	     ytotaj  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:ytotaj]}.compact) # Ingreso total
	     ytothaj  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:ytothaj]}.compact) # Ingreso total hogar
	     ymoneaj  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:ymoneaj]}.compact) # Ingreso monetario
	     yaimhaj  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:yaimhaj]}.compact) # Alquiler imputado, hogar
	     r10  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r10]}.compact) # Vehículo particular (1,2 o 9)
	     r11a  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r11a]}.compact) # Cantidad de vehículos para uso laboral
	     r11b  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r11b]}.compact) # Cantidad de vehículos para uso particular
	     r12  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r12]}.compact) # Seguro para su vehículo (excl SOAP)
	     r13a  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r13a]}.compact) # Tiene Lavadora automática (1,2 o 9)
	     r13b  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r13b]}.compact) # Refrigerador
	     r13c  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r13c]}.compact) # Calefont
	     r13d  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r13d]}.compact) # Teléfono fijo
	     r13e  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r13e]}.compact) # TV de pago
	     r13f  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r13f]}.compact) # Computador
	     r14a  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r14a]}.compact) # B.Ancha fija contrato)
	     r14b  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r14b]}.compact) # (B.Ancha fija prepago)
	     r14c  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r14c]}.compact) # (B.Ancha móvil contrato)
	     r14d  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r14d]}.compact) # (B.Ancha móvil prepago)
	     r14e  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r14e]}.compact) # (smartphone)
	     r16  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r16]}.compact) # Donde utiliza internet 
	     r17  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r17]}.compact) # Frecuencia de uso de internet
	     r18b  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18b]}.compact) # Uso de internet para comunicacion escrita
	     r18c  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18c]}.compact) # Uso de internet para comunicación por voz
	     r18d  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18d]}.compact) # Uso de internet para entretenimiento
	     r18e  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18e]}.compact) # Uso de internet para comercio electrónico
	     r18f  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18f]}.compact) # Uso de internet para banca electrónica
	     r18g  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18g]}.compact) # Uso de internet para capacitación
	     r18h  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r18h]}.compact) # Uso de internet para tramites públicos
	     r19  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r19]}.compact) # Teléfono móvil en uso 
	     r20  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:r20]}.compact) # Cuán satisfecho está con su vida (1 al 10)
	     v1  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v1]}.compact) # Cantidad de viviendas en el sitio (0 al 23)
	     v2  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v2]}.compact) # Metros cuadrados que tiene el sitio
	     v2frent  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v2frent]}.compact) # Metros de frente
	     v2fo  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v2fo]}.compact) # Metros de fondo
	     v3  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v3]}.compact) # Ocupacion del sitio
	     v4  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v4]}.compact) # responsable del sitio
	     v5  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v5]}.compact) # Metros cuadrados de la vivienda
	     v5m  = DescriptiveStatistics::Stats.new(casen.map{|u| u[:v5m]}.compact) # Metros cuadrados de la vivienda

		municipio.edad_mean = unless edad.mean.nil? || edad.mean == 0; edad.mean; end
		municipio.edad_median = edad.median
		municipio.edad_standard_deviation = edad.standard_deviation
		municipio.ecivil_mode = ecivil.mode
		municipio.hombre_percentage =  unless sexo.count(1).nil? || sexo.count(1) == 0; sexo.count(1).to_f*100/sexo.count.to_f; end
		municipio.mujer_percentage = unless sexo.count(2).nil? || sexo.count(2) == 0; sexo.count(2).to_f*100/sexo.count.to_f; end
		municipio.soltero_percentage = unless sexoecivil.count([1,7]).nil? || sexoecivil.count([1,7]) == 0; sexoecivil.count([1,7]).to_f*100/sexo.count(1).to_f; end
		municipio.soltera_percentage = unless sexoecivil.count([2,7]).nil? || sexoecivil.count([2,7]) == 0; sexoecivil.count([2,7]).to_f*100/sexo.count(2).to_f; end
		municipio.casado_percentage = unless sexoecivil.count([1,1]).nil? || sexoecivil.count([1,1]) == 0; sexoecivil.count([1,1]).to_f*100/sexo.count(1).to_f; end
		municipio.casada_percentage = unless sexoecivil.count([2,1]).nil? || sexoecivil.count([2,1]) == 0; sexoecivil.count([2,1]).to_f*100/sexo.count(2).to_f; end
		municipio.emparejado_percentage = unless sexoecivil.count([1,2]).nil? || sexoecivil.count([1,2]) == 0; sexoecivil.count([1,2]).to_f*100/sexo.count(1).to_f; end
		municipio.emparejada_percentage = unless sexoecivil.count([2,2]).nil? || sexoecivil.count([2,2]) == 0; sexoecivil.count([2,2]).to_f*100/sexo.count(2).to_f; end
		municipio.nucleo_mean = unless nucleo.mean.nil?; nucleo.mean; end
		municipio.nucleo_median = unless nucleo.median.nil?; nucleo.median; end
		municipio.nucleo_standard_deviation = nucleo.standard_deviation
		municipio.pco2_mode = pco2.mode
		municipio.numper_mean = numper.mean
		municipio.numper_median = numper.median
		municipio.numper_standard_deviation = numper.standard_deviation
		municipio.e6a_mode = e6a.mode
		municipio.e7_mean = e7.mean
		municipio.e7_median = e7.median
		municipio.e7_standard_deviation = e7.standard_deviation
		municipio.e7_mode = e7.mode
		# municipio.esc_mean = esc.mean
		# municipio.esc_median = esc.median
		# municipio.esc_standard_deviation = esc.standard_deviation
		# municipio.esc_mode = esc.mode
		municipio.educ_mean = educ.mean
		municipio.educ_median = educ.median
		municipio.educ_standard_deviation = educ.standard_deviation
		municipio.educ_mode = educ.mode
		municipio.asiste_mean = asiste.mean
		municipio.asiste_median = asiste.median
		municipio.asiste_standard_deviation = asiste.standard_deviation



		  municipio.tot_hogviv_mean = tot_hogviv.mean # personas que viven en la vivienda
		  municipio.y29a_percentage = unless y29a.count(1).nil? || y29a.count(1)==0; y29a.count(1).to_f*100/y29a.count.to_f; end # Mantiene ahorro 1=si, 2= no, 9=NS/NR
		  municipio.y29b1_percentage = unless y29b1.count(1).nil?|| y29b1.count(1)==0; y29b1.count(1).to_f*100/y29b1.count.to_f; end # Ahorro en cuenta para la vivinda
		  municipio.y29b2_percentage  = unless y29b2.count(1).nil? || y20a.count(1)==0; y29b2.count(1).to_f*100/y29b2.count.to_f; end # Ahorro en cta bancaria
		  municipio.y29b3_percentage = unless y29b3.count(1).nil? || y29b3.count(1)==0; y29b3.count(1).to_f*100/y29b3.count.to_f; end # APV
		  municipio.y29b4_percentage = unless y29b4.count(1).nil? || y29b4.count(1)==0; y29b4.count(1).to_f*100/y29b4.count.to_f; end # Ahorro en efectivo
		  municipio.y20a_percentage  = unless y20a.count(1).nil? || y20a.count(1)==0; y20a.count(1).to_f*100/y20a.count.to_f; end # Prestamos bancarios
		  municipio.y30b_percentage  = unless y30b.count(1).nil? || y30b.count(1)==0; y30b.count(1).to_f*100/y30b.count.to_f; end # Creditos en cajas de compensación
		  municipio.y30c_percentage  = unless y30c.count(1).nil? || y30c.count(1)==0; y30c.count(1).to_f*100/y30c.count.to_f; end # Avance en efectivo casas comerciales
		  municipio.y30d_percentage  = unless y30d.count(1).nil? || y30d.count(1)==0; y30d.count(1).to_f*100/y30d.count.to_f; end # Préstamo amigos y parientes
		  municipio.y30e_percentage  = unless y30e.count(1).nil? || y30e.count(1)==0; y30e.count(1).to_f*100/y30e.count.to_f; end # Crédito con prestamistas o fiado
		  municipio.corte_percentage  = unless corte.count(1).nil? || corte.count(1)==0; corte.count(1).to_f*100/corte.count.to_f; end # Pobres extremos = 1, Pobres no ext=2
		  municipio.qaut_mean  = qaut.mean # Quintil de ingreso autónomo (1 al 5)
		  municipio.daut_mean  = daut.mean # Decil de ingreso autónomo (1 al 10)
		  municipio.yataj_mean  = yataj.mean # Ingreso autónomo
		  municipio.yathaj_mean  = yathaj.mean # Ingreso autónomo, hogar
		  municipio.ytotaj_mean  = unless ytotaj.mean.nil?; ytotaj.mean; end # Ingreso total
		  municipio.ytothaj_mean  = ytothaj.mean # Ingreso total hogar
		  municipio.ymoneaj_mean  = ymoneaj.mean # Ingreso monetario
		  municipio.yaimhaj_mean  = yaimhaj.mean # Alquiler imputado, hogar
		  municipio.r10_percentage  = unless r10.count(1).nil? || r10.count(1)==0; r10.count(1).to_f*100/r10.count.to_f; end # Vehículo particular (1,2 o 9)
		  municipio.r11a_percentage  = unless r11a.count(1).nil? || r11a.count(1)==0; r11a.count(1).to_f*100/r11a.count.to_f; end # Cantidad de vehículos para uso laboral
		  municipio.r11b_percentage  = unless r11b.count(1).nil? || r11b.count(1)==0; r11b.count(1).to_f*100/r11b.count.to_f; end # Cantidad de vehículos para uso particular
		  municipio.r12_percentage  = unless r12.count(1).nil? || r12.count(1)==0; r12.count(1).to_f*100/r12.count.to_f; end # Seguro para su vehículo (excl SOAP)
		  municipio.r13a_percentage  = unless r13a.count(1).nil? || r13a.count(1)==0; r13a.count(1).to_f*100/r13a.count.to_f; end # Tiene Lavadora automática (1,2 o 9)
		  municipio.r13b_percentage  = unless r13b.count(1).nil? || r13b.count(1)==0; r13b.count(1).to_f*100/r13b.count.to_f; end # Refrigerador
		  municipio.r13c_percentage  = unless r13c.count(1).nil? || r13c.count(1)==0; r13c.count(1).to_f*100/r13c.count.to_f; end # Calefont
		  municipio.r13d_percentage  = unless r13d.count(1).nil? || r13d.count(1)==0; r13d.count(1).to_f*100/r13d.count.to_f; end # Teléfono fijo
		  municipio.r13e_percentage  = unless r13e.count(1).nil? || r13e.count(1)==0; r13e.count(1).to_f*100/r13e.count.to_f; end # TV de pago
		  municipio.r13f_percentage  = unless r13f.count(1).nil? || r13f.count(1)==0; r13f.count(1).to_f*100/r13f.count.to_f; end # Computador
		  municipio.r14a_percentage  = unless r14a.count(1).nil? || r14a.count(1)==0; r14a.count(1).to_f*100/r14a.count.to_f; end # B.Ancha fija contrato)
		  municipio.r14b_percentage  = unless r14b.count(1).nil? || r14b.count(1)==0; r14b.count(1).to_f*100/r14b.count.to_f; end # (B.Ancha fija prepago)
		  municipio.r14c_percentage  = unless r14c.count(1).nil? || r14c.count(1)==0; r14c.count(1).to_f*100/r14c.count.to_f; end # (B.Ancha móvil contrato)
		  municipio.r14d_percentage  = unless r14d.count(1).nil? || r14d.count(1)==0; r14d.count(1).to_f*100/r14d.count.to_f; end # (B.Ancha móvil prepago)
		  municipio.r14e_percentage  = unless r14e.count(1).nil? || r14e.count(1)==0; r14e.count(1).to_f*100/r14e.count.to_f; end # (smartphone)
		  municipio.r16_mode  = r16.mode # Donde utiliza internet 
		  municipio.r17_mean  = r17.mean # Frecuencia de uso de internet
		  municipio.r18b_percentage  = unless r18b.count(1).nil? || r18b.count(1)==0; r18b.count(1).to_f*100/r18b.count.to_f; end # Uso de internet para comunicacion escrita
		  municipio.r18c_percentage  = unless r18c.count(1).nil? || r18c.count(1)==0; r18c.count(1).to_f*100/r18c.count.to_f; end # Uso de internet para comunicación por voz
		  municipio.r18d_percentage  = unless r18d.count(1).nil? || r18d.count(1)==0; r18d.count(1).to_f*100/r18d.count.to_f; end # Uso de internet para entretenimiento
		  municipio.r18e_percentage  = unless r18e.count(1).nil? || r18e.count(1)==0; r18e.count(1).to_f*100/r18e.count.to_f; end # Uso de internet para comercio electrónico
		  municipio.r18f_percentage  = unless r18f.count(1).nil? || r18f.count(1)==0; r18f.count(1).to_f*100/r18f.count.to_f; end # Uso de internet para banca electrónica
		  municipio.r18g_percentage  = unless r18g.count(1).nil? || r18g.count(1)==0; r18g.count(1).to_f*100/r18g.count.to_f; end # Uso de internet para capacitación
		  municipio.r18h_percentage  = unless r18h.count(1).nil? || r18h.count(1)==0; r18h.count(1).to_f*100/r18h.count.to_f; end # Uso de internet para tramites públicos
		  municipio.r19_percentage  = unless r19.count(1).nil? || r19.count(1)==0; r19.count(1).to_f*100/r19.count.to_f; end # Teléfono móvil en uso 
		  municipio.r20_mean  = unless r20.mean.nil?; r20.mean; end # Cuán satisfecho está con su vida (1 al 10)
		  municipio.v1_mean  = unless v1.mean.nil?; v1.mean; end # Cantidad de viviendas en el sitio (0 al 23)
		  municipio.v2_mean  = unless v2.mean.nil?; v2.mean; end # Metros cuadrados que tiene el sitio
		  municipio.v2frent_mean  = v2frent.mean # Metros de frente
		  municipio.v2fo_mean  = v2fo.mean # Metros de fondo
		  municipio.v3_percentage  = unless v3.count(1).nil? || v3.count(1)==0; v3.count(1).to_f*100/v3.count.to_f; end # Ocupacion del sitio
		  municipio.v4_percentage  = unless v4.count(1).nil? || v4.count(1)==0; v4.count(1).to_f*100/v4.count.to_f; end # responsable del sitio
		  municipio.v5_mean  = unless v5.mean.nil?; v5.mean; end # Metros cuadrados de la vivienda
		  municipio.v5m_mean  = unless v5m.mean.nil?; v5m.mean; end # Metros cuadrados de la vivienda

		municipio.save
		
		GC.start	
	end

end
