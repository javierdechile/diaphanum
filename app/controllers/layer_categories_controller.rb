class LayerCategoriesController < ApplicationController
  # GET /layer_categories
  # GET /layer_categories.json
  def index
    @layer_categories = LayerCategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.json #{ render json: @layer_categories }
    end
  end

  # GET /layer_categories/1
  # GET /layer_categories/1.json
  def show
    @layer_category = LayerCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @layer_category }
    end
  end

  # GET /layer_categories/new
  # GET /layer_categories/new.json
  def new
    @layer_category = LayerCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @layer_category }
    end
  end

  # GET /layer_categories/1/edit
  def edit
    @layer_category = LayerCategory.find(params[:id])
  end

  # POST /layer_categories
  # POST /layer_categories.json
  def create
    @layer_category = LayerCategory.new(params[:layer_category])

    respond_to do |format|
      if @layer_category.save
        format.html { redirect_to @layer_category, notice: 'Layer category was successfully created.' }
        format.json { render json: @layer_category, status: :created, location: @layer_category }
      else
        format.html { render action: "new" }
        format.json { render json: @layer_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /layer_categories/1
  # PUT /layer_categories/1.json
  def update
    @layer_category = LayerCategory.find(params[:id])

    respond_to do |format|
      if @layer_category.update_attributes(params[:layer_category])
        format.html { redirect_to @layer_category, notice: 'Layer category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @layer_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /layer_categories/1
  # DELETE /layer_categories/1.json
  def destroy
    @layer_category = LayerCategory.find(params[:id])
    @layer_category.destroy

    respond_to do |format|
      format.html { redirect_to layer_categories_url }
      format.json { head :no_content }
    end
  end
end
