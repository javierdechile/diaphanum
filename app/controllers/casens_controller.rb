class CasensController < ApplicationController
  # GET /casens
  # GET /casens.json
  def index
    @casens = Casen.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @casens }
    end
  end

  # GET /casens/1
  # GET /casens/1.json
  def show
    @casen = Casen.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @casen }
    end
  end

  # GET /casens/new
  # GET /casens/new.json
  def new
    @casen = Casen.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @casen }
    end
  end

  # GET /casens/1/edit
  def edit
    @casen = Casen.find(params[:id])
  end

  # POST /casens
  # POST /casens.json
  def create
    @casen = Casen.new(params[:casen])

    respond_to do |format|
      if @casen.save
        format.html { redirect_to @casen, notice: 'Casen was successfully created.' }
        format.json { render json: @casen, status: :created, location: @casen }
      else
        format.html { render action: "new" }
        format.json { render json: @casen.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /casens/1
  # PUT /casens/1.json
  def update
    @casen = Casen.find(params[:id])

    respond_to do |format|
      if @casen.update_attributes(params[:casen])
        format.html { redirect_to @casen, notice: 'Casen was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @casen.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /casens/1
  # DELETE /casens/1.json
  def destroy
    @casen = Casen.find(params[:id])
    @casen.destroy

    respond_to do |format|
      format.html { redirect_to casens_url }
      format.json { head :no_content }
    end
  end
end
